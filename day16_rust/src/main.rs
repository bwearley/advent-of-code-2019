use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day16(&filename).unwrap();
}

fn ones(value: i64) -> i64 {
    value.abs() % 10
}

fn pattern(repeats: usize, min_length: usize) -> Vec<i64> {
    let base: Vec<i64> = vec![0, 1, 0, -1];
    let repeated_base: Vec<i64> = base.iter().flat_map(|x| vec![*x; repeats]).collect();
    repeated_base
        .iter()
        .cycle()
        .take(min_length + 1)
        .copied()
        .skip(1)
        .collect()
}

fn fft(input: &Vec<i64>, phases: usize) -> Vec<i64> {
    let mut values = input.to_owned();
    for _ in 0..phases {
        let mut values_new = vec![0; values.len()];
        for ix in 0..values.len() {
            let pattern = pattern(ix+1, values.len());
            for jx in 0..values.len() {
                values_new[ix] += values[jx] * pattern[jx];
            }
            values_new[ix] = ones(values_new[ix]);
        }
        values = values_new;
    }
    values
}

fn fft_part2(input: &Vec<i64>, phases: usize, offset: usize) -> Vec<i64> {
    let mut values = input.to_owned();
    let len = values.len();
    let mut values_new = vec![0; values.len()];
    for ph in 0..phases {
        println!("Progress {}%", ph);
        for ix in (offset..values.len()).rev() {
            let y = &mut values_new;
            y[ix] = values[ix..len].iter().sum::<i64>() % 10;
        }
        for i in offset..len {
            values[i] = values_new[i];
        }
    }
    values
}

fn fft_fast_part2(input: &Vec<i64>, phases: usize) -> Vec<i64> {
    let mut values = input.to_owned();
    let len = values.len();
    let mut values_new = vec![0; values.len()];
    for ph in 0..phases {
        println!("Progress {}%", ph);
        for ix in (0..values.len()).rev() {
            let y = &mut values_new;
            y[ix] = values[ix..len].iter().sum::<i64>() % 10;
        }
        values[..len].clone_from_slice(&values_new[..len]);
    }
    values
}

fn day16(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let input = input.concat();
    let input: Vec<i64> = input
        .chars()
        .map(|x| x.to_digit(10).unwrap() as i64)
        .collect();

    // Tests
    assert!(fft(&[1,2,3,4,5,6,7,8].to_vec(),4) == [0,1,0,2,9,4,9,8].to_vec());
    assert!(fft(&[8,0,8,7,1,2,2,4,5,8,5,9,1,4,5,4,6,6,1,9,0,8,3,2,1,8,6,4,5,5,9,5].to_vec(),100)[0..8] == [2,4,1,7,6,1,7,6]);
    assert!(fft(&[1,9,6,1,7,8,0,4,2,0,7,2,0,2,2,0,9,1,4,4,9,1,6,0,4,4,1,8,9,9,1,7].to_vec(),100)[0..8] == [7,3,7,4,5,4,1,8]);
    assert!(fft(&[6,9,3,1,7,1,6,3,4,9,2,9,4,8,6,0,6,3,3,5,9,9,5,9,2,4,3,1,9,8,7,3].to_vec(),100)[0..8] == [5,2,4,3,2,1,3,3]);

    // Part 1
    let part1: String = fft(&input,100)[0..8]
        .to_vec()
        .iter()
        .map(|x| x.to_string())
        .collect::<Vec<String>>()
        .concat();
    
    println!("Part 1: {}", part1); // 52611030

    // Part 2
    let offset: usize = input[0..7]
        .to_vec()
        .iter()
        .map(|x| x.to_string())
        .collect::<Vec<String>>()
        .concat()
        .parse::<usize>()
        .unwrap();

    let real_signal = input
        .iter()
        .cycle()
        .take(10_000 * input.len())
        .skip(offset)
        .copied()
        .collect();
    let part2: String = fft_fast_part2(&real_signal,100)[0..8]
        .to_vec()
        .iter()
        .map(|x| x.to_string())
        .collect::<Vec<String>>()
        .concat();
    /*
    let part2: String = fft_part2(&real_signal,100,offset)[offset..offset_end]
        .to_vec()
        .iter()
        .map(|x| x.to_string())
        .collect::<Vec<String>>()
        .concat();*/

    println!("Part 2: {}", part2); // 52541026

    Ok(())
}