#include <iostream>
#include <fstream>
#include <vector>

#include "../shared_cpp/stringtools.cpp"
#include "../intcode_cpp/intcode.cpp"

void Day25(std::string file) {

    std::ifstream in(file);
    std::vector<int64_t> input;
    // Load file data
    while (!in.eof()) {
        std::string line;
        std::getline(in, line);
        auto numStr = split(line,',');
        for (auto & num : numStr) {
            input.push_back(std::stol(num));
        }
    }

    // Initialize & Configure VM
    auto vm = IntcodeComputer(input);
    vm.break_on_out = false;
    vm.break_on_in = true;

    // Part 1
    vm.push_ascii_line("east");
    vm.push_ascii_line("north");
    vm.push_ascii_line("north");
    vm.push_ascii_line("take spool of cat6");
    vm.push_ascii_line("south");
    vm.push_ascii_line("east");
    vm.push_ascii_line("take mug");
    vm.push_ascii_line("north");
    vm.push_ascii_line("north");
    vm.push_ascii_line("west");
    vm.push_ascii_line("take asterisk");
    vm.push_ascii_line("south");
    vm.push_ascii_line("take monolith");
    vm.push_ascii_line("north");
    vm.push_ascii_line("east");
    vm.push_ascii_line("south");
    vm.push_ascii_line("east");
    vm.push_ascii_line("take sand");
    vm.push_ascii_line("south");
    vm.push_ascii_line("west");
    vm.push_ascii_line("take prime number");
    vm.push_ascii_line("east");
    vm.push_ascii_line("north");
    vm.push_ascii_line("east");
    vm.push_ascii_line("north");
    vm.push_ascii_line("south");
    vm.push_ascii_line("south");
    vm.push_ascii_line("take tambourine");
    vm.push_ascii_line("west");
    vm.push_ascii_line("take festive hat");
    vm.push_ascii_line("north");
    vm.push_ascii_line("inv");

    // Manual Inventory
    //vm.push_ascii_line("take prime number");
    //vm.push_ascii_line("take asterisk");
    //vm.push_ascii_line("take sand");
    //vm.push_ascii_line("take tambourine");
    vm.push_ascii_line("drop monolith");
    vm.push_ascii_line("drop spool of cat6");
    vm.push_ascii_line("drop mug");
    vm.push_ascii_line("drop festive hat");

    vm.push_ascii_line("west");
    vm.run();

    vm.stream_ascii(); // 2228740
}

int main(int argc, char** argv) {
    // Check for input file
    if (argc != 2) {
        printf("Must specify file at command line.\n");
        return -1;
    }

    Day25(argv[1]);
    return 0;
}