'use strict';

const fs = require('fs');
const chalk = require('chalk');

var args = process.argv.slice(2);

function getFuel1(fuel) {
    return Math.floor(fuel / 3) - 2;
}

function getFuel2(fuel) {
    var total = 0;
    var partial = Math.floor(fuel / 3) - 2;
    if (partial > 0) { total += partial + getFuel2(partial); }
    return total;
}

function day01(filename) {
    const input = fs.readFileSync(filename)
        .toString()
        .split("\n")
        .filter(s => s.length > 0)
        .map(line => Number(line));

    var part1 = input.map(line => getFuel1(line)).reduce((a,b) => a+b,0);
    var part2 = input.map(line => getFuel2(line)).reduce((a,b) => a+b,0);
    return [part1, part2];
}

const [part1,part2] = day01(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 3563458
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 5342292