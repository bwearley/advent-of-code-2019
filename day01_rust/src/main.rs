use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day01(&filename).unwrap();
}

fn get_fuel_part1(mass: i64) -> i64 {
    (mass / 3) - 2
}

fn get_fuel_part2(mass: i64) -> i64 {
    let mut total = 0;
    let partial = (mass / 3) - 2;
    if partial > 0 { total += partial + get_fuel_part2(partial); }
    total
}

fn day01(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let input: Vec<i64> = input
        .iter()
        .map(|x| x.parse::<i64>().unwrap())
        .collect();

    // Part 1
    let part1 = input
        .iter()
        .map(|x| get_fuel_part1(*x))
        .sum::<i64>();
    println!("Part 1: {}", part1); // 3563458

    // Part 2
    let part2 = input
        .iter()
        .map(|x| get_fuel_part2(*x))
        .sum::<i64>();
    println!("Part 2: {}", part2); // 5342292

    Ok(())
}