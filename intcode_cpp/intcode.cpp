#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <deque>

class IntcodeComputer {
    public:
        std::deque<int64_t> input_stack;
        std::deque<int64_t> output_stack;
        int64_t ptr;
        int64_t rel_base;
        std::vector<int64_t> intcode;
        std::vector<int64_t> intcode_default;
        bool active;
        bool debug;
        bool break_on_out;
        bool break_on_in;

        IntcodeComputer(std::vector<int64_t> intcode_in) {
            // Allocate additional memory for program
            auto code = intcode_in;
            std::vector<int64_t> buffer(1024 * 10, 0);
            code.insert(code.end(), buffer.begin(), buffer.end());

            ptr = 0;
            rel_base = 0;
            intcode = code;
            intcode_default = code;
            active = true;
            debug = false;
            break_on_out = true;
            break_on_in = false;
        }

        void push_input(int64_t input) {
            input_stack.emplace_back(input);
        }

        int64_t pop_output() {
            int64_t out = output_stack.front();
            output_stack.pop_front();
            return out;
        }

        int64_t last_output() {
            return output_stack.back();
        }

        void reset() {
            intcode = intcode_default;
            ptr = 0;
            rel_base = 0;
            active = true;
            input_stack.clear();
            output_stack.clear();
        }

        int64_t get_param(int64_t position, int64_t mode) {
            return intcode[get_dest(position,mode)];
        }

        int64_t get_dest(int64_t position, int64_t mode) {
            switch (mode) {
                case 0: return intcode[position];
                case 1: return position;
                case 2: return intcode[position] + rel_base;
                default: printf("Unknown parameter type: %lld", mode); exit(EXIT_FAILURE);
            }
        }

        void set_addr(int64_t addr, int64_t to) {
            intcode[addr] = to;
        }

        void stream_ascii() {
            while (output_stack.size() != 0) {
                auto c = output_stack.front();
                output_stack.pop_front();
                if (c >= 0 && c < 127) {
                    std::cout << (char)c;
                } else {
                    std::cout << c;
                }
            }
        }
        
        void push_ascii_line(std::string line) {
            push_ascii_string(line);
            push_input((int64_t)'\n');
        }
        
        void push_ascii_string(std::string line) {
            for (auto it = line.cbegin(); it != line.cend(); ++it) {
                push_input((int64_t)*it);
            }
        }

        void run() {
            int64_t a, b, d;
            while (true) {
                auto instr = intcode[ptr];
                auto digits = digits_reversed(instr).digits;
                auto opcode = digits[1] * 10 + digits[0];
                switch(opcode) {
                    // Add
                    case 1:
                        a = get_param(ptr+1, digits[2]);
                        b = get_param(ptr+2, digits[3]);
                        d = get_dest( ptr+3, digits[4]);
                        if (debug) { printf("add(%lld:%lld) %lld %lld->%lld\n", opcode, instr, a, b, d); }
                        intcode[d] = a + b;
                        ptr += 4;
                        break;
                    
                    // Multiply
                    case 2:
                        a = get_param(ptr+1, digits[2]);
                        b = get_param(ptr+2, digits[3]);
                        d = get_dest( ptr+3, digits[4]);
                        if (debug) { printf("mult(%lld:%lld) %lld %lld->%lld\n", opcode, instr, a, b, d); }
                        intcode[d] = a * b;
                        ptr += 4;
                        break;
                    
                    // Input
                    case 3:
                        if (input_stack.size() == 0 && break_on_in) { return; }
                        a = input_stack.front(); 
                        d = get_dest(ptr+1, digits[2]);
                        if (debug) { printf("inp(%lld:%lld) %lld->%lld\n", opcode, instr, a, d); }
                        intcode[d] = a;
                        input_stack.pop_front();
                        ptr += 2;
                        break;
                    
                    // Output
                    case 4:
                        a = get_param(ptr+1, digits[2]);
                        if (debug) { printf("out(%lld:%lld) %lld\n", opcode, instr, a); }
                        output_stack.emplace_back(a);
                        ptr += 2;
                        if (break_on_out) return;
                        break;
                    
                    // Jump if True
                    case 5:
                        a = get_param(ptr+1, digits[2]);
                        b = get_param(ptr+2, digits[3]);
                        if (debug) { printf("jit(%lld:%lld) %lld %lld\n", opcode, instr, a, b); }
                        (a != 0) ? ptr = b : ptr += 3;
                        break;
                    
                    // Jump if False
                    case 6:
                        a = get_param(ptr+1, digits[2]);
                        b = get_param(ptr+2, digits[3]);
                        if (debug) { printf("jif(%lld:%lld) %lld %lld\n", opcode, instr, a, b); }
                        (a == 0) ? ptr = b : ptr += 3;
                        break;
                    
                    // Less Than
                    case 7:
                        a = get_param(ptr+1, digits[2]);
                        b = get_param(ptr+2, digits[3]);
                        d = get_dest( ptr+3, digits[4]);
                        if (debug) { printf("lt(%lld:%lld) %lld %lld->%lld\n", opcode, instr, a, b, d); }
                        (a < b) ? intcode[d] = 1 : intcode[d] = 0;
                        ptr += 4;
                        break;
                    
                    // Equal
                    case 8:
                        a = get_param(ptr+1, digits[2]);
                        b = get_param(ptr+2, digits[3]);
                        d = get_dest( ptr+3, digits[4]);
                        if (debug) { printf("eq(%lld:%lld) %lld %lld->%lld\n", opcode, instr, a, b, d); }
                        (a == b) ? intcode[d] = 1 : intcode[d] = 0;
                        ptr += 4;
                        break;

                    // Adjust Base
                    case 9:
                        a = get_param(ptr+1, digits[2]);
                        if (debug) { printf("adjb(%lld:%lld) %lld\n", opcode, instr, a); }
                        rel_base += a;
                        ptr += 2;
                        break;

                    // Halt
                    case 99:
                        active = false;
                        if (debug) { printf("EXT(%lld)\n", instr); }
                        return;

                    default:
                        printf("ERROR: Unknown instruction: %lld\n", intcode[ptr]);
                        exit(EXIT_FAILURE);
                        break;
                }
            }

        }

        struct digits {
            int64_t digits[5];
        };
        digits digits_reversed(int64_t n) {
            digits d;
            d.digits[0] = n / 1     % 10;
            d.digits[1] = n / 10    % 10;
            d.digits[2] = n / 100   % 10;
            d.digits[3] = n / 1000  % 10;
            d.digits[4] = n / 10000 % 10;
            return d;
        }

};