'use strict';

const chalk = require('chalk');
const fs = require('fs');

var args = process.argv.slice(2);

function day04(filename) {
    const input = fs.readFileSync(filename).toString();
    const [start,end] = input.split("-").map(n => Number(n));

    var part1 = 0;
    var part2 = 0;
    for (var pw = start; pw <= end; pw++) {
        if (validate_part1(pw)) part1 += 1;
        if (validate_part2(pw)) part2 += 1;
    }

    return [part1, part2];
}

function validate_part1(input) {
    const digits = input.toString().split("").map(n => Number(n));

    // Check never decreases
    for (var i = 1; i < digits.length; i++) {
        if (digits[i] < digits[i-1]) { return false; }
    }

    // Check for existence of pair
    for (var i = 1; i < digits.length; i++) {
        if (digits[i] == digits[i-1]) { return true; }
    }
}

function validate_part2(input) {
    const digits = input.toString().split("").map(n => Number(n));

    // Check never decreases
    for (var i = 1; i < digits.length; i++) {
        if (digits[i] < digits[i-1]) { return false; }
    }

    // Check for existence of ONLY a pair
    if (input.toString().match(/(?:^|(.)(?!\1))(\d)\2(?!\2)/)) return true;
    return false;
}

const [part1,part2] = day04(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 1855
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 1253