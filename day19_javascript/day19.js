'use strict';
const fs = require('fs');
const chalk = require('chalk');
const Hashmap = require('hashmap');

var {IntcodeComputer} = require('../intcode_js/intcode.js');

var args = process.argv.slice(2); 

function part2Ans(x,y) {
    return x*10000+y;
}

function check(vm, x, y) {
    vm.reset();
    vm.push_input(x);
    vm.push_input(y);
    vm.run();
    return vm.pop_output() === 1;
}

function day19(filename) {

    const MAX_XDIM = 50;
    const MAX_YDIM = 50;
    const BOX = 99;

    // Load input
    const input = fs.readFileSync(filename)
        .toString()
        .split(",")
        .map(n => Number(n));
    
    // Initialize & Configure VM
    var vm = new IntcodeComputer(input);

    // Initialize Map State
    var img = new Hashmap();

    // Process 50x50
    for (var y = 0; y < MAX_YDIM; y++) {
        for (var x = 0; x < MAX_XDIM; x++) {
            if (check(vm,x,y)) {
                img.set([x,y],true);
            } else {
                img.set([x,y],false);
            }
        }
    }
    const part1 = img.values().filter(e => e).length;

    // Draw
    const DRAW_IMG = true;
    if (DRAW_IMG) {
        for (var y = 0; y < MAX_YDIM; y++) {
            for (var x = 0; x < MAX_XDIM; x++) {
                if (img.get([x,y])) {
                    process.stdout.write("#");
                } else {
                    process.stdout.write(".");
                }
            }
            console.log();
        }
    }

    // Part 2
    var bl = [3,2]; // starting point of lower beam
    var part2 = 0;
    while (true) {
        var [x0,y0] = bl;
        if (check(vm,x0    ,y0-BOX) &&
            check(vm,x0+BOX,y0-BOX)) {
            //console.log(`Ans=(${x0},${y0-BOX})`)
            part2 = part2Ans(x0, y0-BOX);
            break;
        }
        if (check(vm, x0+1, y0+1)) {
            bl = [x0+1, y0+1];
        } else {
            bl = [x0+2, y0+1];
        }
    }

    return [part1,part2];
}

const [part1,part2] = day19(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 199
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 10180726