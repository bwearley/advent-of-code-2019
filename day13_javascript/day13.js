'use strict';
const fs = require('fs');
const chalk = require('chalk');
const Hashmap = require('hashmap');
//const Clear = require('clear');

var {IntcodeComputer} = require('../intcode_js/intcode.js');

var args = process.argv.slice(2); 

function chunk(array, size) {
    const chunked_arr = [];
    for (let i = 0; i < array.length; i++) {
      const last = chunked_arr[chunked_arr.length - 1];
      if (!last || last.length === size) {
        chunked_arr.push([array[i]]);
      } else {
        last.push(array[i]);
      }
    }
    return chunked_arr;
}

const EMPTY = 0;
const WALL = 1;
const BLOCK = 2;
const PADDLE = 3;
const BALL = 4;

const X_IX = 0;
const Y_IX = 1;
const ID_I = 2;

function day13(filename) {

    const DRAW_MAP = true;

    // Load input
    const input = fs.readFileSync(filename)
        .toString()
        .split(",")
        .map(n => Number(n));
    
    // Initialize & Configure VM
    var vm = new IntcodeComputer(input);
    vm.break_on_out = false;
    vm.break_on_in = true;
    //vm.debug = true;
    vm.setAddr(0,2); // insert quarters

    // Init Game
    var part1 = 0;
    var img = new Hashmap();
    var score = 0;

    // Run Game
    while (vm.active) {
        vm.run();
       
        var outputs = [];
        while (vm.output_stack.length != 0) {
            outputs.push(vm.output_stack.first());
            vm.output_stack.shift();
        }

        const batches = chunk(outputs,3);
        for (const chnk of batches) {
            var x = chnk[X_IX];
            var y = chnk[Y_IX];
            var d = chnk[ID_I];
            //console.log(` ${x} ${y} ${d}`);
            if (x == -1 && y == 0) {
                score = d;
            } else {
                img.set([x,y],d);
            }
        }

        // Calculate Part 1
        if (part1 == 0) part1 = img.entries().filter(e => e[1] == BLOCK).length;

        // Get image limits
        const xmax = Math.max( ...img.keys().map(k => k[0]) );
        const xmin = Math.min( ...img.keys().map(k => k[0]) ); 
        const ymax = Math.max( ...img.keys().map(k => k[1]) );
        const ymin = Math.min( ...img.keys().map(k => k[1]) );

        // Draw
        //Clear();
        if (DRAW_MAP) {
            console.log(chalk.blue("Map:"));
            console.log(chalk.green("Score: ") + chalk.yellow(score))
            for (var y = ymin; y <= ymax; y++) {
                for (var x = xmin; x <= xmax; x++) {
                    switch (img.get([x,y])) {
                        case EMPTY:
                            process.stdout.write(" ");
                            break;
                        case WALL:
                            process.stdout.write("#");
                            break;
                        case BLOCK:
                            process.stdout.write("*");
                            break;
                        case PADDLE:
                            process.stdout.write("=");
                            break;
                        case BALL:
                            process.stdout.write("O");
                            break;
                        default:
                            process.stdout.write(" ");
                            break;
                    }
                }
                console.log();
            }
        }

        // Get ball & paddle positions
        var ballx = Number(img.entries().filter(e => e[1] == BALL).map(e => e[0][X_IX]));
        var paddlex = Number(img.entries().filter(e => e[1] == PADDLE).map(e => e[0][X_IX]));

        // Submit joystick input
        vm.push_input(Math.sign(ballx - paddlex));
    }
    return [part1,score];
}

const [part1,part2] = day13(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 329
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 15973