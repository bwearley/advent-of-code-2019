#include <iostream>
#include <fstream>
#include <vector>

int getFuel1(int mass) {
    return (mass / 3) - 2;
}

int getFuel2(int mass) {
    int total = 0;
    int partial = (mass / 3) - 2;
    if (partial > 0) { total += partial + getFuel2(partial); }
    return total;
}

void Day01(std::string file)
{
    std::ifstream in(file);
    std::vector<int> input;
    // Load file data
    while (!in.eof())
    {
        std::string line;
        std::getline(in, line);
        if (!in.fail())
        {
            input.push_back(std::stoi(line));
        }
    }

    // Part 1
    int part1 = 0;
    for (auto & num : input) {
        part1 += getFuel1(num);
    }
    printf("Part 1: %d\n", part1); // 3563458

    // Part 2
    int part2 = 0;
    for (auto & num : input) {
        part2 += getFuel2(num);
    }
    printf("Part 2: %d\n", part2); // 5342292

}


int main(int argc, char** argv)
{
  // Check for input file
  if (argc != 2) {
    printf("Must specify file at command line.\n");
    return -1;
  }

  Day01(argv[1]);
  return 0;
}