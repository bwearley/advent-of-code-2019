'use strict';
const fs = require('fs');
const chalk = require('chalk');

var args = process.argv.slice(2);

var orbits = [];
class Orbit {
    constructor(s) {
        const parts = s.split(")");
        this.root  = parts[0];
        this.orbit = parts[1];
    }
}

function num_orbits(start,num) {
    var num_in = num + 1;
    var next = orbits.filter(o => o.root == start);
    for (const o of next) {
        num += num_orbits(o.orbit,num_in);
    }
    return num;
}

function transfers(start) {
    var path = [];
    path.push(start);
    if (start == "COM") return path;
    var next = orbits.filter(o => o.orbit == start)[0];
    for (const p of transfers(next.root)) {
        path.push(p);
    }
    return path;
}

function day06(filename) {

    // Load input
    const input = fs.readFileSync(filename)
        .toString()
        .split("\n")
        .filter(s => s.length > 0);

    orbits = input.map(s => new Orbit(s));

    // Part 1
    const part1 = num_orbits("COM",0);

    // Part 2
    // Get path from YOU->COM and from SAN->COM
    // Remove elements in common to both arrays
    // Subtract 2 because YOU and SAN are in the
    // path but don't count.
    const part2_1 = transfers("YOU");
    const part2_2 = transfers("SAN");
    const part2 = part2_1.filter(v => part2_2.indexOf(v) == -1).length 
                + part2_2.filter(v => part2_1.indexOf(v) == -1).length
                - 2;

    return [part1,part2];
}
  
const [part1,part2] = day06(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 333679
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 370