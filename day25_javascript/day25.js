'use strict';
const fs = require('fs');
const chalk = require('chalk');

var {IntcodeComputer} = require('../intcode_js/intcode.js');

var args = process.argv.slice(2); 

var combine = function(a, min, max) {
    var fn = function(n, src, got, all) {
        if (n == 0) {
            if (got.length > 0) {
                all[all.length] = got;
            }
            return;
        }
        for (var j = 0; j < src.length; j++) {
            fn(n - 1, src.slice(j + 1), got.concat([src[j]]), all);
        }
        return;
    }
    var all = [];
    //for (var i = min; i < a.length; i++) {
    for (var i = min; i < max; i++) {
        fn(i, a, [], all);
    }
    all.push(a);
    return all;
}

function day25(filename) {

    // Load input
    const input = fs.readFileSync(filename)
        .toString()
        .split(",")
        .map(n => Number(n));
    
    // Initialize & Configure VM
    var vm = new IntcodeComputer(input);
    vm.break_on_out = false;
    vm.break_on_in = true;

    // Part 1
    vm.pushASCIILine("east");
    vm.pushASCIILine("north");
    vm.pushASCIILine("north");
    vm.pushASCIILine("take spool of cat6");
    vm.pushASCIILine("south");
    vm.pushASCIILine("east");
    vm.pushASCIILine("take mug");
    vm.pushASCIILine("north");
    vm.pushASCIILine("north");
    vm.pushASCIILine("west");
    vm.pushASCIILine("take asterisk");
    vm.pushASCIILine("south");
    vm.pushASCIILine("take monolith");
    vm.pushASCIILine("north");
    vm.pushASCIILine("east");
    vm.pushASCIILine("south");
    vm.pushASCIILine("east");
    vm.pushASCIILine("take sand");
    vm.pushASCIILine("south");
    vm.pushASCIILine("west");
    vm.pushASCIILine("take prime number");
    vm.pushASCIILine("east");
    vm.pushASCIILine("north");
    vm.pushASCIILine("east");
    vm.pushASCIILine("north");
    vm.pushASCIILine("south");
    vm.pushASCIILine("south");
    vm.pushASCIILine("take tambourine");
    vm.pushASCIILine("west");
    vm.pushASCIILine("take festive hat");
    vm.pushASCIILine("north");
    vm.pushASCIILine("inv");

    var items = "prime number,spool of cat6,festive hat,monolith,mug,asterisk,sand,tambourine".split(",");
    for (const inv of combine(items,3,8)) {
        for (const item of items) {
            vm.pushASCIILine(`drop ${item}`);
        }
        for (const item of inv) {
            vm.pushASCIILine(`take ${item}`);
        }
        vm.pushASCIILine("west");
        vm.run();
        if (!vm.active) break;
    }

    vm.streamASCII(); // 2228740
}

day25(args[0]);