#[macro_use]
extern crate text_io;

pub mod point3d {
    use std::ops::{Add,AddAssign};

    #[derive(Debug, Copy, Clone, Hash)]
    pub struct Point3D {
        pub x: i64,
        pub y: i64,
        pub z: i64,
    }

    impl Point3D {
        pub fn new(x: i64, y: i64, z: i64) -> Point3D {
            Point3D {
                x: x, y: y, z: z,
            }
        }
        pub fn zeros() -> Point3D {
            Point3D {
                x: 0, y: 0, z: 0,
            }
        }
    }
    impl Add for Point3D {
        type Output = Self;
        fn add(self, other: Self) -> Self {
            Self {
                x: self.x + other.x,
                y: self.y + other.y,
                z: self.z + other.z,
            }
        }
    }
    impl AddAssign for Point3D {
        fn add_assign(&mut self, other: Self) {
            *self = Self {
                x: self.x + other.x,
                y: self.y + other.y,
                z: self.z + other.z,
            };
        }
    }
    impl PartialEq for Point3D {
        fn eq(&self, other: &Self) -> bool {
            self.x == other.x && self.y == other.y && self.z == other.z
        }
    }
    impl Eq for Point3D {}

}

// #[cfg(test)]
// mod tests {
//     #[test]
//     fn it_works() {
//         assert_eq!(2 + 2, 4);
//     }
// }
