use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::collections::HashSet;

const DIM: i64 = 5;
const NUM_MINUTES: usize = 200;

fn get_neighbors(x: i64, y: i64) -> Vec<(i64,i64)> {
    let mut neighbors: Vec<(i64,i64)> = Vec::new();
    if x-0 >=  0 && y-1 >=  0 { neighbors.push( (x-0,y-1) ); }
    if x-1 >=  0 && y-0 >=  0 { neighbors.push( (x-1,y-0) ); }
    if x+1 < DIM && y-0 >=  0 { neighbors.push( (x+1,y-0) ); }
    if x-0 >=  0 && y+1 < DIM { neighbors.push( (x-0,y+1) ); }
    neighbors
}

fn get_recursive_neighbors(x: i64, y: i64, d: i64) -> Vec<(i64,i64,i64)> {
    let mut neighbors: Vec<(i64,i64,i64)> = Vec::new();
    if x == 0 { neighbors.push( (1,2,d-1) ); } // cell 12
    if x == 4 { neighbors.push( (3,2,d-1) ); } // cell 14
    if y == 0 { neighbors.push( (2,1,d-1) ); } // cell 8
    if y == 4 { neighbors.push( (2,3,d-1) ); } // cell 18

    if x == 2 && y == 1 {
        // cells ABCDE of d+1
        neighbors.push( (0,0,d+1) );
        neighbors.push( (1,0,d+1) );
        neighbors.push( (2,0,d+1) );
        neighbors.push( (3,0,d+1) );
        neighbors.push( (4,0,d+1) );
    }
    if x == 1 && y == 2 {
        // cells AFKPU of d+1
        neighbors.push( (0,0,d+1) );
        neighbors.push( (0,1,d+1) );
        neighbors.push( (0,2,d+1) );
        neighbors.push( (0,3,d+1) );
        neighbors.push( (0,4,d+1) );
    }
    if x == 3 && y == 2 {
        // cells EJOTY of d+1
        neighbors.push( (4,0,d+1) );
        neighbors.push( (4,1,d+1) );
        neighbors.push( (4,2,d+1) );
        neighbors.push( (4,3,d+1) );
        neighbors.push( (4,4,d+1) );
    }
    if x == 2 && y == 3 {
        // cells UVWXY of d+1
        neighbors.push( (0,4,d+1) );
        neighbors.push( (1,4,d+1) );
        neighbors.push( (2,4,d+1) );
        neighbors.push( (3,4,d+1) );
        neighbors.push( (4,4,d+1) );
    }

    if x-0 >=  0 && y-1 >=  0 { neighbors.push( (x-0,y-1,d) ); }
    if x-1 >=  0 && y-0 >=  0 { neighbors.push( (x-1,y-0,d) ); }
    if x+1 < DIM && y-0 >=  0 { neighbors.push( (x+1,y-0,d) ); }
    if x-0 >=  0 && y+1 < DIM { neighbors.push( (x-0,y+1,d) ); }
    neighbors
}
/*
     |     |         |     |     
  1  |  2  |    3    |  4  |  5  
     |     |         |     |     
-----+-----+---------+-----+-----
     |     |         |     |     
  6  |  7  |    8    |  9  |  10 
     |     |         |     |     
-----+-----+---------+-----+-----
     |     |A|B|C|D|E|     |     
     |     |-+-+-+-+-|     |     
     |     |F|G|H|I|J|     |     
     |     |-+-+-+-+-|     |     
 11  | 12  |K|L|?|N|O|  14 |  15 
     |     |-+-+-+-+-|     |     
     |     |P|Q|R|S|T|     |     
     |     |-+-+-+-+-|     |     
     |     |U|V|W|X|Y|     |     
-----+-----+---------+-----+-----
     |     |         |     |     
 16  | 17  |    18   |  19 |  20 
     |     |         |     |     
-----+-----+---------+-----+-----
     |     |         |     |     
 21  | 22  |    23   |  24 |  25 
     |     |         |     |     

    Tile 19 has four adjacent tiles: 14, 18, 20, and 24.
    Tile G has four adjacent tiles: B, F, H, and L.
    Tile D has four adjacent tiles: 8, C, E, and I.
    Tile E has four adjacent tiles: 8, D, 14, and J.
    Tile 14 has eight adjacent tiles: 9, E, J, O, T, Y, 15, and 19.
    Tile N has eight adjacent tiles: I, O, S, and five tiles within the sub-grid marked ?.

*/

fn biodiversity(map: &[Vec<i64>]) -> i32 {
    let mut bio: i32 = 0;
    let mut ix: i32 = 0;
    for y in 0..DIM as usize {
        for x in 0..DIM as usize {
            if map[x][y] == 1 { bio += (2 as i32).pow(ix as u32); }
            ix += 1;
        }
    }
    bio
}

fn part1(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    // Inital bug map
    let mut bugs = vec![vec![0; DIM as usize]; DIM as usize];
    for y in 0..input.len() as usize {
        let mut line = input[y].chars();
        for x in 0..DIM as usize {
            bugs[x][y] = if line.next().unwrap() == '#' { 1 } else { 0 };
        }
    }

    let mut seen: HashSet<Vec<Vec<i64>>> = HashSet::new();

    'main_lp: loop {
        let current = bugs.clone();

        for y in 0..DIM as usize {
            for x in 0..DIM as usize {

                // Count neighbors that are bugs
                let neighbors = get_neighbors(x as i64, y as i64);
                let mut num_bugs = 0;
                for neighbor in neighbors {
                    let (xn,yn) = (neighbor.0 as usize, neighbor.1 as usize);
                    if current[xn][yn] == 1 { num_bugs += 1; }
                }

                // An empty space becomes infested with a bug if exactly one or two bugs are adjacent to it.
                if current[x][y] == 0 && (num_bugs == 1 || num_bugs == 2) {
                    bugs[x][y] = 1;
                }

                // A bug dies (becoming an empty space) unless there is exactly one bug adjacent to it.
                if current[x][y] == 1 && num_bugs != 1 {
                    bugs[x][y] = 0;
                }
            }
        }
        // Check if previously seen
        match seen.get(&bugs) {
            Some(_) => { break 'main_lp; },
            None => { seen.insert(bugs.clone()); },
        }
    }

    println!("Part 1: {}", biodiversity(&bugs)); // 32506764

    Ok(())
}

fn part2(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    const BASE_LEVEL: usize = 200;
    const NUM_LEVELS: usize = 2 * BASE_LEVEL;

    // Inital bug map
    let mut bugs = vec![vec![vec![0; NUM_LEVELS as usize]; DIM as usize]; DIM as usize];
    for y in 0..input.len() as usize {
        let mut line = input[y].chars();
        for x in 0..DIM as usize {
            bugs[x][y][BASE_LEVEL] = if line.next().unwrap() == '#' { 1 } else { 0 };
        }
    }

    for _ in 0..NUM_MINUTES {
        let current = bugs.clone();

        for z in 1..NUM_LEVELS-1 as usize {
            for y in 0..DIM as usize {
                for x in 0..DIM as usize {
                    if x == 2 && y == 2 { continue; }

                    // Count neighbors that are bugs
                    let neighbors = get_recursive_neighbors(x as i64, y as i64, z as i64);
                    let mut num_bugs = 0;
                    for neighbor in neighbors {
                        let (xn,yn,zn) = (neighbor.0 as usize, neighbor.1 as usize, neighbor.2 as usize);
                        if current[xn][yn][zn] == 1 { num_bugs += 1; }
                    }

                    // An empty space becomes infested with a bug if exactly one or two bugs are adjacent to it.
                    if current[x][y][z] == 0 && (num_bugs == 1 || num_bugs == 2) {
                        bugs[x][y][z] = 1;
                    }

                    // A bug dies (becoming an empty space) unless there is exactly one bug adjacent to it.
                    if current[x][y][z] == 1 && num_bugs != 1 {
                        bugs[x][y][z] = 0;
                    }
                }
            }
        }
    }
    // Calculate number of bugs
    let part2: i64 = bugs
        .iter()
        .flat_map(|x| x.iter().flatten())
        .sum();

    println!("Part 2: {}", part2); // 1963

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    part1(&filename).unwrap();
    part2(&filename).unwrap();
}