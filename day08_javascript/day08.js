'use strict';

const fs = require('fs');
const chalk = require('chalk');

var args = process.argv.slice(2);

function chunk(array, size) {
    const chunked_arr = [];
    for (let i = 0; i < array.length; i++) {
      const last = chunked_arr[chunked_arr.length - 1];
      if (!last || last.length === size) {
        chunked_arr.push([array[i]]);
      } else {
        last.push(array[i]);
      }
    }
    return chunked_arr;
}

const count = (array,value) => {
    return array.filter(e => e == value).length;
};

Array.prototype.minBy = function(lambda) {
    var lambdaFn;
    if (typeof(lambda) === "function") {
        lambdaFn = lambda;
    } else {
        lambdaFn = function(x){
            return x[lambda];
        }
    }
    var mapped = this.map(lambdaFn); 
    var minValue = Math.min.apply(Math, mapped); 
    return this[mapped.indexOf(minValue)];
}

function day08(filename) {

    const input = fs.readFileSync(filename)
        .toString()
        .split("\n")
        .filter(s => s.length != 0) // Messy way to deal with line break becoming 0
        .flatMap(c => c.split("").map(n => Number(n)));

    // Image Dimensions
    const width = 25; //3 //(test input)
    const height = 6; //2 //(test input)
    const size = width * height;

    let layers = chunk(input, size);

    // Part 1
    let p1 = layers.minBy(l => count(l,0));
    let part1 = p1.filter(e => e == 1).length * p1.filter(e => e == 2).length;
    console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 828

    // Construct Image
    // 0 = black
    // 1 = white
    // 2 = transparent
    var pixels = new Array();
    for (var i = 0; i < width; i++) {
        pixels[i] = new Array(height).fill(2);
    }

    for (const layer of layers) {
        var rows = chunk(layer, width);
        for (var y = 0; y < height; y++) {
            for (var x = 0; x < width; x++) {
                if (pixels[x][y] == 2) {
                    pixels[x][y] = rows[y][x];
                }
            }
        }
    }

    // Draw Image
    console.log(chalk.blue("Part 2:"));
    for (var y = 0; y < height; y++) {
        for (var x = 0; x < width; x++) {
            switch (pixels[x][y]) {
                case 0:
                    process.stdout.write(" ");
                    break;
                case 1:
                    process.stdout.write("#");
                    break;
                case 2:
                    process.stdout.write(" ");
                    break;
            }
        }
        console.log();
    }
}

day08(args[0]);
// Part 1: 828
// Part 2: ZLBJF