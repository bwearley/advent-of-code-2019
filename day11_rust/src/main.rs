use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::collections::HashMap;

use intcode::intcode::IntcodeComputer;
extern crate intcode;

extern crate num_complex;
use num_complex::Complex;

extern crate point2d;
use point2d::point2d::Point2D;

#[derive(Copy, Clone)]
enum PaintColor {
    Black = 0,
    White = 1,
}

fn day11(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let input = input.concat();
    let intcode = input
        .split(',')
        .map(|x| x.parse::<i64>().unwrap())
        .collect::<Vec<i64>>();

    let turn_left  = Complex::new(0, -1);
    let turn_right = Complex::new(0, 1);
 
    // Initialize & Configure VM
    let mut vm = IntcodeComputer::new(intcode);
    vm.push_input(PaintColor::Black as i64); // starts on black

    // Initalize Map/State
    let mut img: HashMap<Point2D, PaintColor> = HashMap::new();
    let mut pos = Complex::new(0, 0);
    let mut dir = Complex::new(0, -1);
    img.insert(Point2D::from_complex(pos), PaintColor::Black);

    // Run robot
    loop {
        vm.run();
        if !vm.active { break; }
        let color = match vm.pop_output() {
            0 => PaintColor::Black,
            1 => PaintColor::White,
            other => panic!("Unknown paint color: {}", other),
        };
        vm.run();
        if !vm.active { break; }
        match vm.pop_output() {
            0 => dir *= turn_left,
            1 => dir *= turn_right,
            other => panic!("Unknown direction: {}", other),
        }
        img.insert(Point2D::from_complex(pos),color);

        // Move
        pos += dir;

        // Start next
        let next = match img.get(&Point2D::from_complex(pos)) {
            Some(c) => *c as i64,
            None => PaintColor::Black as i64,
        };
        vm.push_input(next);
    }
    println!("Part 1: {}", img.len()); // 2184
 
    // Reset and reinitialize VM
    vm.reset();
    vm.push_input(PaintColor::White as i64); // starts on white

    // Clear map and reset state
    img.clear();
    pos = Complex::new(0, 0);
    dir = Complex::new(0, -1);
    img.insert(Point2D::from_complex(pos), PaintColor::White);

    // Run robot
    loop {
        vm.run();
        if !vm.active { break; }
        let color = match vm.pop_output() {
            0 => PaintColor::Black,
            1 => PaintColor::White,
            other => panic!("Unknown paint color: {}", other),
        };
        vm.run();
        if !vm.active { break; }
        match vm.pop_output() {
            0 => dir *= turn_left,
            1 => dir *= turn_right,
            other => panic!("Unknown direction: {}", other),
        }
        img.insert(Point2D::from_complex(pos),color);
        
        // Move
        pos += dir;

        // Start next
        let next = match img.get(&Point2D::from_complex(pos)) {
            Some(c) => *c as i64,
            None => PaintColor::Black as i64,
        };
        vm.push_input(next);
    }

    // Get image limits
    let xmin = img.keys().map(|pt| pt.x).min().unwrap();
    let xmax = img.keys().map(|pt| pt.x).max().unwrap();
    let ymin = img.keys().map(|pt| pt.y).min().unwrap();
    let ymax = img.keys().map(|pt| pt.y).max().unwrap();

    // Draw
    println!("Part 2:"); // AHCHZEPK
    for y in ymin..=ymax {
        for x in xmin..=xmax {
            match img.get(&Point2D::new(x,y)) {
                Some(PaintColor::Black) => print!(" "),
                Some(PaintColor::White) => print!("#"),
                None => print!(" "),
            }
        }
        println!();
    }
    
    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day11(&filename).unwrap();
}