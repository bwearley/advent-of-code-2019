use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

const WIDTH: usize = 25;
const HEIGHT: usize = 6;
const SIZE: usize = WIDTH * HEIGHT;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day08(&filename).unwrap();
}

fn day08(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let input = input.concat();

    let layers = input
        .chars()
        .map(|c| c.to_digit(10).unwrap() as u64)
        .collect::<Vec<_>>();
    let layers = layers.chunks(SIZE);

    let min_zeros_layer = layers
        .clone()
        .min_by(|a,b| 
            a.iter().filter(|x| *x == &0).count().cmp(
           &b.iter().filter(|y| *y == &0).count())
        )
        .unwrap();

    let part1 = 
        min_zeros_layer.iter().filter(|e| *e == &1).count() *
        min_zeros_layer.iter().filter(|e| *e == &2).count();
    println!("Part 1: {}", part1); // 828

    // Construct Image
    // 0 = black
    // 1 = white
    // 2 = transparent
    let mut pixels = vec![vec![2; HEIGHT]; WIDTH];
    for layer in layers {
        let mut rows = layer.chunks(WIDTH);
        for y in 0..HEIGHT {
            let row = rows.next().unwrap();
            for x in 0..WIDTH {
                if pixels[x][y] == 2 {
                    pixels[x][y] = row[x];
                }
            }
        }
    }

    println!("Part 2:"); // ZLBJF
    for y in 0..HEIGHT {
        for x in 0..WIDTH {
            match pixels[x][y] {
                0 => print!(" "),
                1 => print!("#"),
                2 => print!(" "),
                other => panic!("Unknwon character {}", other),
            }
        }
        println!();
    }

    Ok(())
}