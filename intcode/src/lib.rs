pub mod intcode {
    use std::collections::VecDeque;

    #[derive(Clone)]
    pub struct IntcodeComputer {
        pub input_stack: VecDeque<i64>,
        pub output_stack: VecDeque<i64>,
        pub ptr: i64,
        pub rel_base: i64,
        pub intcode: Vec<i64>,
        intcode_default: Vec<i64>,
        pub active: bool,
        // Options
        pub debug: bool,
        pub default_input: Option<i64>,
        pub break_outputs: Option<i64>,
    }
    impl IntcodeComputer {
        pub fn new(intcode: Vec<i64>) -> IntcodeComputer {
            // Allocate additional memory for program
            let mut code = intcode;
            let mut buffer = vec![0; 1024 * 10];
            code.append(&mut buffer);

            IntcodeComputer {
                ptr: 0,
                rel_base: 0,
                intcode: code.clone(),
                intcode_default: code,
                input_stack: VecDeque::new(),
                output_stack: VecDeque::new(),
                active: true,
                // Options
                debug: false,
                default_input: None,
                break_outputs: Some(1),
            }
        }
        pub fn push_input(&mut self, input: i64) {
            self.input_stack.push_back(input);
        }
        pub fn pop_output(&mut self) -> i64 {
            self.output_stack.pop_front().expect("Error: Tried to get first output from empty stack")
        }
        pub fn last_output(&mut self) -> i64 {
            self.output_stack.pop_back().expect("Error: Tried to get last output from empty stack")
        }
        pub fn reset(&mut self) {
            self.intcode = self.intcode_default.clone();
            self.ptr = 0;
            self.rel_base = 0;
            self.active = true;
            self.input_stack.clear();
            self.output_stack.clear();
        }
        fn get_param(&self, position: usize, mode: usize) -> i64 {
            self.intcode[self.get_dest(position, mode)]
        }
        fn get_dest(&self, position: usize, mode: usize) -> usize {
            match mode {
                0 => self.intcode[position] as usize,
                1 => position,
                2 => (self.intcode[position] + self.rel_base) as usize,
                other => panic!("Unknown parameter type: {}", other),
            }
        }
        pub fn get_addr(&mut self, addr: i64) -> i64 {
            self.intcode[addr as usize]
        }
        pub fn set_addr(&mut self, addr: i64, to: i64) {
            self.intcode[addr as usize] = to;
        }
        pub fn stream_ascii(&mut self) {
            while !self.output_stack.is_empty() {
                let c = self.output_stack.pop_front().unwrap();
                match c {
                    0..=126 => print!("{}", c as u8 as char),
                    _ => print!("{}", c),
                }
            }
        }
        pub fn push_ascii_line(&mut self, line: &str) {
            self.push_ascii_string(line);
            self.push_input('\n' as i64);
        }
        pub fn push_ascii_string(&mut self, line: &str) {
            for ch in line.chars() {
                self.push_input(ch as i64);
            }
        }
    
        pub fn run(&mut self) {
            loop {
                let ptr = self.ptr as usize;
                let instr = self.intcode[ptr];             // 54321
                let digits = digits_reversed(instr);       // [1,2,3,4,5]
                let opcode = digits[1] * 10 + digits[0];   // 21
                match opcode {
                    // Add
                    1 => {
                        let a = self.get_param(ptr+1, digits[2]);
                        let b = self.get_param(ptr+2, digits[3]);
                        let d = self.get_dest(ptr+3, digits[4]);
                        if self.debug { println!("add({}:{}) {} {}->{}", opcode, instr, a, b, d); }
                        self.intcode[d] = a + b;
                        self.ptr += 4;
                    },
                    
                    // Multiply
                    2 => {
                        let a = self.get_param(ptr+1, digits[2]);
                        let b = self.get_param(ptr+2, digits[3]);
                        let d = self.get_dest(ptr+3, digits[4]);
                        if self.debug { println!("mult({}:{}) {} {}->{}", opcode, instr, a, b, d); }
                        self.intcode[d] = a * b;
                        self.ptr += 4;
                    },
                    
                    // Input
                    3 => {
                        if self.input_stack.is_empty() {
                            if let Some(v) = self.default_input { self.push_input(v) }
                            break;
                        }
                        let input = self.input_stack.pop_front().expect("Error: Requested input (3) but none provided");
                        let d = self.get_dest(ptr+1, digits[2]);
                        if self.debug { println!("inp({}:{}) {}->{}", opcode, instr, input, d); }
                        self.intcode[d] = input;
                        self.ptr += 2;
                    },
        
                    // Output
                    4 => {
                        let a = self.get_param(ptr+1, digits[2]);
                        if self.debug { println!("out({}:{}) {}", opcode, instr, a); }
                        self.output_stack.push_back(a);
                        self.ptr += 2;
                        if let Some(v) = self.break_outputs {
                            if self.output_stack.len() as i64 == v { break }
                        }
                    },
        
                    // Jump if True
                    5 => {
                        let a = self.get_param(ptr+1, digits[2]);
                        let b = self.get_param(ptr+2, digits[3]);
                        if self.debug { println!("jit({}:{}) {} {}", opcode, instr, a, b); }
                        if a != 0 { self.ptr = b } else { self.ptr += 3 }
                    },
                    
                    // Jump if False
                    6 => {
                        let a = self.get_param(ptr+1, digits[2]);
                        let b = self.get_param(ptr+2, digits[3]);
                        if self.debug { println!("jif({}:{}) {} {}", opcode, instr, a, b); }
                        if a == 0 { self.ptr = b } else { self.ptr += 3 }
                    },
                    
                    // Less Than
                    7 => {
                        let a = self.get_param(ptr+1, digits[2]);
                        let b = self.get_param(ptr+2, digits[3]);
                        let d = self.get_dest(ptr+3, digits[4]);
                        if self.debug { println!("lt({}:{}) {} {}->{}", opcode, instr, a, b, d); }
                        if a < b { self.intcode[d] = 1 } else { self.intcode[d] = 0 }
                        self.ptr += 4;
                    },
                    
                    // Equal
                    8 => {
                        let a = self.get_param(ptr+1, digits[2]);
                        let b = self.get_param(ptr+2, digits[3]);
                        let d = self.get_dest(ptr+3, digits[4]);
                        if self.debug { println!("eq({}:{}) {} {}->{}", opcode, instr, a, b, d); }
                        if a == b { self.intcode[d] = 1 } else { self.intcode[d] = 0 }
                        self.ptr += 4;
                    },

                    // Adjust Base
                    9 => {
                        let a = self.get_param(ptr+1, digits[2]);
                        if self.debug { println!("adjb({}:{}) {}", opcode, instr, a); }
                        self.rel_base += a;
                        self.ptr += 2;
                    },

                    // Halt
                    99 => {
                        self.active = false;
                        if self.debug { println!("EXT({})", instr); }
                        break;
                    }

                    other => panic!("ERROR: Unknown instruction: {}", other),
                }
            } 
        }
    }

    fn digits_reversed(n: i64) -> [usize; 5] {
        let n = n as usize;
        [n / 1     % 10,
         n / 10    % 10,
         n / 100   % 10,
         n / 1000  % 10,
         n / 10000 % 10]
    }

}

#[cfg(test)]
mod tests {
    use super::*;

    fn intcode_from_string(input: &str) -> Vec<i64> {
        input.split(",").map(|x| x.parse::<i64>().unwrap()).collect()
    }

    #[test]
    fn day02_test_1() {
        let intcode = intcode_from_string("1,9,10,3,2,3,11,0,99,30,40,50");
        let mut vm = intcode::IntcodeComputer::new(intcode);
        vm.run();
        assert!(vm.get_addr(0) == 3500)
    }
    #[test]
    fn day02_test_2() {
        let intcode = intcode_from_string("2,4,4,5,99,0");
        let mut vm = intcode::IntcodeComputer::new(intcode);
        vm.run();
        assert!(vm.get_addr(5) == 9801)
    }
    #[test]
    fn day09_test_1() {
        let intcode = intcode_from_string("109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99");
        let mut vm = intcode::IntcodeComputer::new(intcode.clone());
        vm.break_outputs = None;
        vm.run();
        assert!(vm.output_stack.iter().cloned().collect::<Vec<i64>>() == intcode)
    }
    #[test]
    fn day09_test_3() {
        let intcode = intcode_from_string("104,1125899906842624,99");
        let mut vm = intcode::IntcodeComputer::new(intcode);
        vm.run();
        assert!(vm.pop_output() == 1125899906842624)
    }
}