//#[macro_use]
//extern crate text_io;

pub mod point2d {
    use std::ops::{Add,AddAssign};
    extern crate num_complex;
    use num_complex::Complex;

    #[derive(Debug, Copy, Clone, Hash)]
    pub struct Point2D {
        pub x: i64,
        pub y: i64,
    }

    impl Point2D {
        pub fn new(x: i64, y: i64) -> Point2D {
            Point2D {
                x: x, y: y,
            }
        }
        pub fn zeros() -> Point2D {
            Point2D {
                x: 0, y: 0,
            }
        }
        pub fn from_complex(cmplx: Complex<i64>) -> Point2D {
            Point2D {
                x: cmplx.re, y: cmplx.im,
            }
        }
        pub fn as_complex(self) -> Complex<i64> {
            Complex::new(self.x, self.y)
        }
    }
    impl Add for Point2D {
        type Output = Self;
        fn add(self, other: Self) -> Self {
            Self {
                x: self.x + other.x,
                y: self.y + other.y,
            }
        }
    }
    impl AddAssign for Point2D {
        fn add_assign(&mut self, other: Self) {
            *self = Self {
                x: self.x + other.x,
                y: self.y + other.y,
            };
        }
    }
    impl PartialEq for Point2D {
        fn eq(&self, other: &Self) -> bool {
            self.x == other.x && self.y == other.y
        }
    }
    impl Eq for Point2D {}

}

// #[cfg(test)]
// mod tests {
//     #[test]
//     fn it_works() {
//         assert_eq!(2 + 2, 4);
//     }
// }
