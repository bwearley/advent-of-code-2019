#include <iostream>
#include <string>
#include <vector>
#include <sstream>

std::vector<std::string> split(const std::string& s, char delimiter)
{
  std::vector<std::string> tokens;
  std::string token;
  std::istringstream tokenStream(s);
  while (std::getline(tokenStream, token, delimiter))
  {
    tokens.push_back(token);
  }
  return tokens;
}

std::string trim(const std::string &s)
{
  auto start = s.begin();
  while (start != s.end() && std::isspace(*start))
  {
    start++;
  }

  auto end = s.end();
  do
  {
    end--;
  } while (std::distance(start,end) > 0 && std::isspace(*end));

  return std::string(start, end+1);
}

// a->0, p->15
auto s2i(std::string a)
{
  auto aChar = a.c_str();
  return aChar[0] - 97;
};

// 0->a, 15->p
auto i2s(int i)
{
  char a = i + 97;
  return a;
};