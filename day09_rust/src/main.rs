use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

use intcode::intcode::IntcodeComputer;
extern crate intcode;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day09(&filename).unwrap();
}

fn day09(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let input = input.concat();
    let intcode = input
        .split(',')
        .map(|x| x.parse::<i64>().unwrap())
        .collect::<Vec<i64>>();

    // Part 1
    let mut comp = IntcodeComputer::new(intcode);
    comp.push_input(1);
    comp.run();
    println!("Part 1: {}", comp.pop_output()); // 3507134798

    // Part 2
    comp.reset();
    comp.push_input(2);
    comp.run();
    println!("Part 2: {}", comp.pop_output()); // 84513
    
    Ok(())
}