'use strict';

const fs = require('fs');
const chalk = require('chalk');

var {IntcodeComputer} = require('../intcode_js/intcode.js');

var args = process.argv.slice(2); 

function day09(filename) {

    // Load input
    const input = fs.readFileSync(filename)
        .toString()
        .split(",")
        .map(n => Number(n));

    var vm = new IntcodeComputer(input);
    
    // Part 1
    vm.reset();
    vm.push_input(1);
    vm.break_on_out = false;
    vm.run();
    const part1 = vm.output_stack.last();

    // Part 2
    vm.reset();
    vm.push_input(2);
    vm.break_on_out = false;
    vm.run();
    const part2 = vm.output_stack.last();

    return [part1,part2];
}
  
const [part1,part2] = day09(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 3507134798
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 84513
