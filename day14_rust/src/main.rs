use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::collections::HashMap;
use std::fmt;

extern crate regex;
use regex::Regex;

extern crate num_integer;
use num_integer::div_ceil;

#[derive(Clone, Hash)]
struct Ingredient {
    pub qty: i64,
    pub name: String,
}
impl Ingredient {
    pub fn new(qty: i64, name: &str) -> Ingredient {
        Ingredient {
            qty: qty,
            name: name.to_string(),
        }
    }
}
impl PartialEq for Ingredient {
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
    }
}
impl Eq for Ingredient {}

struct Recipe {
    pub ingredients: Vec<Ingredient>,
    pub result: Ingredient,
}
impl Recipe {
    pub fn from_string(s: &str) -> Recipe {
        let re = Regex::new(r"(\d+ \w+)+").unwrap();
        let materials: Vec<_> = re
            .captures_iter(s)
            .map(|cap| {
                let parts = &cap[1];
                let parts: Vec<_> = parts.split(' ').collect();
                Ingredient::new(parts[0].parse::<i64>().unwrap(),parts[1])
            })
            .collect();
        if let Some((result,ingredients)) = materials.split_last() {
            Recipe {
                ingredients: ingredients.to_vec(),
                result: result.clone(),
            }
        } else {
            panic!("Error unpacking recipe: {}", s);
        }
    }
}
impl fmt::Display for Recipe {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for ingr in &self.ingredients {
            write!(f, "{} {} ", ingr.qty, ingr.name);
        }
        write!(f, " -> {} {}", self.result.qty, self.result.name)
    }
}

fn ore_to_make(recipes: &Vec<Recipe>, surplus: &mut HashMap<String,i64>, product: &str, qty: i64) -> i64 {
    let recipe = recipes.iter().find(|r| r.result.name == product).unwrap();
    let existing: i64 = match surplus.get(product) {
        Some(s) => *s,
        None => 0,
    };
    let multiple = div_ceil(std::cmp::max(qty - existing,0) , recipe.result.qty);
    let extra = (recipe.result.qty * multiple) - (qty - existing);
    if product != "ORE" { surplus.insert(product.to_string(),extra); }
    let mut ore = 0;
    for ingr in &recipe.ingredients {
        if ingr.name == "ORE" {
            ore += multiple * ingr.qty;
        } else {
            ore += ore_to_make(&recipes, surplus, &ingr.name, multiple * ingr.qty);
        }
    }
    ore
}

fn day14(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    let recipes: Vec<_> = input.iter().map(|line| Recipe::from_string(line)).collect();

    /*println!("All Recipes:");
    for r in &recipes {
        println!("{}", r);
    }*/

    // Part 1
    let mut surplus: HashMap<String,i64> = HashMap::new();
    let part1 = ore_to_make(&recipes, &mut surplus, "FUEL", 1);
    println!("Part 1: {}", part1); // 216477

    // Part 2
    const ORE_STORAGE: i64 = 1_000_000_000_000;
    let mut part2 = 0;
    for i in 11_780_000.. {
        surplus.clear();
        if ore_to_make(&recipes, &mut surplus, "FUEL", i) <= ORE_STORAGE {
            part2 = i;
        } else {
            break;
        }
    }
    println!("Part 2: {}", part2); // 11788286

    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day14(&filename).unwrap();
}