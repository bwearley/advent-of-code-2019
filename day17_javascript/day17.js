'use strict';
const fs = require('fs');
const chalk = require('chalk');
const mathjs = require('mathjs');
const Hashmap = require('hashmap');

var {IntcodeComputer} = require('../intcode_js/intcode.js');

var args = process.argv.slice(2); 

const ASCII = {
    SCAFFOLD: "#".charCodeAt(0),
    OPEN:     ".".charCodeAt(0),
    NEWLINE: "\n".charCodeAt(0),
    ROBOT_U:  "^".charCodeAt(0),
    ROBOT_D:  "v".charCodeAt(0),
    ROBOT_L:  "<".charCodeAt(0),
    ROBOT_R:  ">".charCodeAt(0),
    A:        "A".charCodeAt(0),
    B:        "B".charCodeAt(0),
    C:        "C".charCodeAt(0),
};

function tupleFromComplex(cmplx) {
    return [cmplx.re,cmplx.im];
}

function day17(filename) {

    // Load input
    const input = fs.readFileSync(filename)
        .toString()
        .split(",")
        .map(n => Number(n));
    
    // Initialize & Configure VM
    var vm = new IntcodeComputer(input);
    vm.break_on_out = false;
    vm.run();

    // Initialize Map State
    var img = new Hashmap();

    // Collect VM Outputs
    var outputs = [];
    while (vm.output_stack.length != 0) {
        outputs.push(vm.output_stack.first());
        vm.output_stack.shift();
    }

    // Draw Map (Initial)
    const DRAW_INITIAL_MAP = false;
    var x = 0;
    var y = 0;
    for (const o of outputs) {
        switch (o) {
            case ASCII.NEWLINE:
                x = 0;
                y += 1;
                if (DRAW_INITIAL_MAP) console.log();
                break;
            default:
                if (DRAW_INITIAL_MAP) process.stdout.write(String.fromCharCode(o));
                img.set([x,y],o);
                x += 1;
                break;
        }
    }

    // Get intersections
    var inters = new Hashmap();
    for (const [k,v] of img.entries()) {
        let [x,y] = k;
        let up    = img.get([x  ,y+1]);
        let down  = img.get([x  ,y-1]);
        let left  = img.get([x-1,y  ]);
        let right = img.get([x+1,y  ]);
        if (v === ASCII.SCAFFOLD &&
            up === v && down === v &&
            left === v && right === v) {
            inters.set(k,v);
        }
    }

    // Calculate Part 1
    const part1 = inters.keys().map(k => k[0]*k[1]).reduce((a,b) => a+b,0);

    // Draw Map (w/ Intersections)
    const DRAW_INTERSECTIONS_MAP = false;
    if (DRAW_INTERSECTIONS_MAP) {
        var x = 0;
        var y = 0;
        for (const o of outputs) {
            switch (o) {
                case ASCII.NEWLINE:
                    x = 0;
                    y += 1;
                    console.log();
                    break;
                default:
                    if (inters.get([x,y]) == ASCII.SCAFFOLD) {
                        process.stdout.write("O");
                    } else {
                        process.stdout.write(String.fromCharCode(o));
                    }
                    x += 1;
                    break;
            }
        }
    }

    // Get path
    /*
    var [x,y] = img.search(ASCII.ROBOT_U);
    var pos = mathjs.complex(x,y);
    var dir = mathjs.complex(0,-1);
    console.log(`Start: (${pos})`)
    const turn_left = mathjs.complex(0, -1);
    const turn_right = mathjs.complex(0, +1);
    var path = [];
    var mag = 0;
    while (true) {
        var next = tupleFromComplex(mathjs.add(pos, dir));
        if (img.get(next) != ASCII.SCAFFOLD) {
            path.push(String(mag));
            mag = 0;
            // Determine turn
            var left = tupleFromComplex(mathjs.add(pos, mathjs.multiply(dir, turn_left)));
            var right = tupleFromComplex(mathjs.add(pos, mathjs.multiply(dir, turn_right)));
            if (img.get(left) == ASCII.SCAFFOLD) {
                dir = mathjs.multiply(dir, turn_left);
                pos = mathjs.add(pos, dir);
                mag += 1;
                path.push("L");
            } else if (img.get(right) == ASCII.SCAFFOLD) {
                dir = mathjs.multiply(dir, turn_right);
                pos = mathjs.add(pos, dir);
                mag += 1;
                path.push("R");
            } else {
                path.push(String(mag));
                break;
            }
        } else {
            mag += 1;
            pos = mathjs.add(pos, dir);
        }
    }

    // Print full path
    path.shift(); path.pop(); // trim first and last
    console.log("Path:")
    console.log(path.join(","))
    */

    // R,4,L,12,L,8,R,4,L,8,R,10,R,10,R,6,R,4,L,12,L,8,R,4,R,4,R,10,L,12,R,4,L,12,L,8,R,4,L,8,R,10,R,10,R,6,R,4,L,12,L,8,R,4,R,4,R,10,L,12,L,8,R,10,R,10,R,6,R,4,R,10,L,12
    //  A = R,4,L,12,L,8,R,4,
    //  B = L,8,R,10,R,10,R,6,
    //  A = R,4,L,12,L,8,R,4,
    //  C = R,4,R,10,L,12,
    //  A = R,4,L,12,L,8,R,4,
    //  B = L,8,R,10,R,10,R,6,
    //  A = R,4,L,12,L,8,R,4,
    //  C = R,4,R,10,L,12,
    //  B = L,8,R,10,R,10,R,6,
    //  C = R,4,R,10,L,12

    // Run
    vm.reset();
    vm.setAddr(0,2);
    vm.pushASCIILine("A,B,A,C,A,B,A,C,B,C"); // MAIN
    vm.pushASCIILine("R,4,L,12,L,8,R,4"); // FUNCTION_A
    vm.pushASCIILine("L,8,R,10,R,10,R,6"); // FUNCTION_B
    vm.pushASCIILine("R,4,R,10,L,12"); // FUNCTION_C
    vm.pushASCIILine("n");
    vm.run();

    const LIVE_CAMERA = true;
    const part2 = vm.output_stack.last();
    if (LIVE_CAMERA) { vm.streamASCII(); console.log(); }
    
    return [part1,part2];
}

const [part1,part2] = day17(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 3936
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 785733