'use strict';
const fs = require('fs');
const chalk = require('chalk');

var {IntcodeComputer} = require('../intcode_js/intcode.js');

var args = process.argv.slice(2); 

function day02(filename) {

    // Load input
    const input = fs.readFileSync(filename)
        .toString()
        .split(",")
        .map(n => Number(n));

    var vm = new IntcodeComputer(input);

    // Part 1
    vm.setAddr(1,12);
    vm.setAddr(2,2);
    vm.run();
    const part1 = vm.getAddr(0);

    // Part 2
    var part2 = 0;
    for (var noun = 0; noun <= 99; noun++) {
        for (var verb = 0; verb <= 99; verb++) {
            vm.reset();
            vm.setAddr(1,noun);
            vm.setAddr(2,verb);
            vm.run();
            if (vm.getAddr(0) == 19690720) {
                part2 = 100 * noun + verb;
                break;
            }
        }
        if (part2 != 0) break;
    }

    return [part1,part2];
}
  
const [part1,part2] = day02(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 7210630
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 3892