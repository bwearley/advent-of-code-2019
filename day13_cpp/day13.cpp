#include <iostream>
#include <fstream>
#include <vector>
#include <map>

#include "../shared_cpp/stringtools.cpp"
#include "../intcode_cpp/intcode.cpp"

typedef std::int64_t i64;
typedef std::tuple<i64,i64> pt2d;

auto point(i64 x, i64 y, i64 d) {
    return std::pair<pt2d,i64>(pt2d(x,y),d);
}

template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

enum MapTile {
    Empty  = 0,
    Wall   = 1,
    Block  = 2,
    Paddle = 3,
    Ball   = 4,
    Score  = 10,
};

void Day13(std::string file) {

    const bool DRAW_MAP = true;

    std::ifstream in(file);
    std::vector<i64> input;
    // Load file data
    while (!in.eof()) {
        std::string line;
        std::getline(in, line);
        auto numStr = split(line,',');
        for (auto & num : numStr) {
            input.push_back(std::stol(num));
        }
    }

    // Initialize & Configure VM
    auto vm = IntcodeComputer(input);
    vm.break_on_out = false;
    vm.break_on_in = true;
    //vm.debug = true;
    vm.set_addr(0,2); // insert quarters

    // Initialize Map State
    i64 part1 = 0;
    std::map<pt2d,i64> img;
    i64 score = 0;

    while (vm.active) {
        vm.run();
        for (auto i = 0; i < vm.output_stack.size(); i+=3) {
            auto x = vm.output_stack[i+0];
            auto y = vm.output_stack[i+1];
            auto d = vm.output_stack[i+2];
            if (x == -1 && y == 0) {
                score = d;
            } else {
                auto res = img.insert( point(x,y,d) );
                if (!res.second) res.first->second = d;
            }
        }
        vm.output_stack.clear();

        // Calculate Part 1
        if (part1 == 0) {
            for (auto it = img.begin(); it != img.end(); ++it) {
                if (it->second == MapTile::Block) part1 += 1;
            }
        }

        const bool DRAW_MAP = true;
        if (DRAW_MAP) {
            // Get image limits
            i64 xmin = INT64_MAX;
            i64 ymin = INT64_MAX;
            i64 xmax = INT64_MIN;
            i64 ymax = INT64_MIN;
            for (auto it = img.begin(); it != img.end(); ++it) {
                xmin = std::min(xmin, std::get<0>(it->first));
                ymin = std::min(ymin, std::get<1>(it->first));
                xmax = std::max(xmax, std::get<0>(it->first));
                ymax = std::max(ymax, std::get<1>(it->first));
            }
            printf("Map:\n");
            printf("Score: %lld\n", score);
            for (auto y = ymin; y <= ymax; y++) {
                for (auto x = xmin; x <= xmax; x++) {
                    auto it = img.find(pt2d(x,y));
                    if (it != img.end()) {
                        switch (it->second) {
                            case MapTile::Empty:  printf(" "); break;
                            case MapTile::Wall:   printf("#"); break;
                            case MapTile::Block:  printf("*"); break;
                            case MapTile::Paddle: printf("="); break;
                            case MapTile::Ball:   printf("O"); break;
                            case MapTile::Score:  printf(" "); break;
                            default:              printf(" "); break;
                        }
                    } 
                }
                std::cout << std::endl;
            }
        }

        // Get ball & paddle positions
        auto ballx   = std::get<0>(std::find_if(img.begin(), img.end(),
            [&](const std::pair<pt2d,i64> &pt) {
                return pt.second == MapTile::Ball;
            })->first);
        auto paddlex = std::get<0>(std::find_if(img.begin(), img.end(),
            [&](const std::pair<pt2d,i64> &pt) {
                return pt.second == MapTile::Paddle;
            })->first);

        // Submit joystick input
        vm.push_input(sgn(ballx-paddlex));
    }

    printf("Part 1: %lld\n", part1); // 329
    printf("Part 2: %lld\n", score); // 15973

}

int main(int argc, char** argv) {
    // Check for input file
    if (argc != 2) {
        printf("Must specify file at command line.\n");
        return -1;
    }

    Day13(argv[1]);
    return 0;
}