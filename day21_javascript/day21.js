'use strict';
const fs = require('fs');
const chalk = require('chalk');

var {IntcodeComputer} = require('../intcode_js/intcode.js');

var args = process.argv.slice(2); 

function day21(filename) {

    // Load input
    const input = fs.readFileSync(filename)
        .toString()
        .split(",")
        .map(n => Number(n));
    
    // Initialize & Configure VM
    var vm = new IntcodeComputer(input);
    vm.break_on_out = false;
    vm.break_on_in = true;

    // Part 1: 19358262
    vm.pushASCIILine("OR A J");
    vm.pushASCIILine("AND B J");
    vm.pushASCIILine("AND C J");
    vm.pushASCIILine("NOT J J");
    vm.pushASCIILine("AND D J");
    vm.pushASCIILine("WALK");
    vm.run();
    vm.streamASCII();
    console.log();

    // Part 2: 1142686742
    vm.reset();
    vm.pushASCIILine("OR A J");
    vm.pushASCIILine("AND B J");
    vm.pushASCIILine("AND C J");
    vm.pushASCIILine("NOT J J");
    vm.pushASCIILine("AND D J");
    vm.pushASCIILine("OR E T");
    vm.pushASCIILine("OR H T");
    vm.pushASCIILine("AND T J");
    vm.pushASCIILine("RUN");
    vm.run();
    vm.streamASCII();
    console.log();
    
}

day21(args[0]);