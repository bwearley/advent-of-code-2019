'use strict';

const dequeue = require('dequeue');

function digitsReversed(n) {
    return [
        Math.floor(n / 1     % 10),
        Math.floor(n / 10    % 10),
        Math.floor(n / 100   % 10),
        Math.floor(n / 1000  % 10),
        Math.floor(n / 10000 % 10)
    ];
}

class IntcodeComputer {
    constructor(intcode) {
        // Allocate additional memory for program
        var buffer = new Array(1024*10).fill(0);
        intcode.push(...buffer);

        this.input_stack = new dequeue();
        this.output_stack = new dequeue();
        this.ptr = 0;
        this.rel_base = 0;
        this.intcode = intcode.slice(0);
        this.intcode_default = intcode.slice(0);
        this.active = true;
        this.debug = false;
        this.break_on_out = true;
        this.break_on_in = false;
        this.neg_1_on_empty_in = true;
        this.break_on_out3 = false;
    }
    push_input(input) {
        this.input_stack.push(input);
    }
    pop_output() {
        return this.output_stack.shift();
    }
    last_output() {
        return this.output_stack.pop();
    }
    reset() {
        this.intcode = this.intcode_default.slice(0);
        this.ptr = 0;
        this.rel_base = 0;
        this.active = true;
        this.input_stack.empty();
        this.output_stack.empty();
    }
    getParam(position, mode) {
        return this.intcode[this.getDest(position,mode)];
    }
    getDest(position, mode) {
        switch (mode) {
            case 0: return this.intcode[position];
            case 1: return position;
            case 2: return this.intcode[position] + this.rel_base;
            default:
                console.log(`Unknown parameter type: ${mode}`);
                this.active = false;
                return;
        }
    }
    getAddr(addr) {
        return this.intcode[addr];
    }
    setAddr(addr,to) {
        this.intcode[addr] = to;
    }
    streamASCII() {
        while (this.output_stack.length != 0) {
            var c = this.output_stack.first();
            if (c >= 0 && c <= 126) {
                process.stdout.write(String.fromCharCode(c));
            } else {
                process.stdout.write(String(c));
            }
            this.output_stack.shift();
        }
    }
    pushASCIILine(s) {
        this.pushASCIIString(s)
        this.pushASCIIString('\n')
    }
    pushASCIIString(s) {
        var p = s.split("").map(c => c.charCodeAt(0));
        for (const c of p) { this.push_input(c); }
    }

    run() {
        while (true) {
            var ptr = this.ptr;
            var instr = this.intcode[ptr];                  // 54321
            var digits = digitsReversed(this.intcode[ptr]); // [1,2,3,4,5]
            var opcode = digits[1] * 10 + digits[0];        // 21
            switch (opcode) {
                // Add
                case 1:
                    var a = this.getParam(ptr+1, digits[2]);
                    var b = this.getParam(ptr+2, digits[3]);
                    var d = this.getDest(ptr+3, digits[4]);
                    if (this.debug) { console.log(`add(${opcode}:${instr}) ${a} ${b}->${d}`); }
                    this.intcode[d] = a + b;
                    this.ptr += 4;
                    break;
                
                // Multiply
                case 2:
                    var a = this.getParam(ptr+1, digits[2]);
                    var b = this.getParam(ptr+2, digits[3]);
                    var d = this.getDest(ptr+3, digits[4]);
                    if (this.debug) { console.log(`mult(${opcode}:${instr}) ${a} ${b}->${d}`); }
                    this.intcode[d] = a * b;
                    this.ptr += 4;
                    break;
                
                // Input
                case 3:
                    if (this.input_stack.length == 0 && this.break_on_in) return;
                    if (this.input_stack.length == 0 && this.neg_1_on_empty_in) {
                        this.push_input(-1);
                        return;
                    }
                    var a = this.input_stack.shift();
                    var d = this.getDest(ptr+1, digits[2]);
                    if (this.debug) { console.log(`inp(${opcode}:${instr}) ${a}->${d}`); }
                    this.intcode[d] = a;
                    this.ptr += 2;
                    break;
                
                // Output
                case 4:
                    var a = this.getParam(ptr+1, digits[2]);
                    if (this.debug) { console.log(`out(${opcode}:${instr}) ${a}`); }
                    this.output_stack.push(a);
                    this.ptr += 2;
                    if (this.break_on_out) { return; }
                    if (this.break_on_out3 && this.output_stack.length == 3) { return; }
                    break;

                // Jump if True
                case 5:
                    var a = this.getParam(ptr+1, digits[2]);
                    var b = this.getParam(ptr+2, digits[3]);
                    if (this.debug) { console.log(`jit(${opcode}:${instr}) ${a} ${b}`); }
                    (a != 0) ? this.ptr = b : this.ptr += 3;
                    break;
                
                // Jump if False
                case 6:
                    var a = this.getParam(ptr+1, digits[2]);
                    var b = this.getParam(ptr+2, digits[3]);
                    if (this.debug) { console.log(`jif(${opcode}:${instr}) ${a} ${b}`); }
                    (a == 0) ? this.ptr = b : this.ptr += 3;
                    break;
                
                // Less Than
                case 7:
                    var a = this.getParam(ptr+1, digits[2]);
                    var b = this.getParam(ptr+2, digits[3]);
                    var d = this.getDest(ptr+3, digits[4]);
                    if (this.debug) { console.log(`lt(${opcode}:${instr}) ${a} ${b}->${d}`); }
                    (a < b) ? this.intcode[d] = 1 : this.intcode[d] = 0;
                    this.ptr += 4;
                    break;
                
                // Equals
                case 8:
                    var a = this.getParam(ptr+1, digits[2]);
                    var b = this.getParam(ptr+2, digits[3]);
                    var d = this.getDest(ptr+3, digits[4]);
                    if (this.debug) { console.log(`eq(${opcode}:${instr}) ${a} ${b}->${d}`); }
                    (a == b) ? this.intcode[d] = 1 : this.intcode[d] = 0;
                    this.ptr += 4;
                    break;

                // Adjust Base
                case 9:
                    var a = this.getParam(ptr+1, digits[2]);
                    if (this.debug) { console.log(`adjb(${opcode}:${instr}) ${a}`); }
                    this.rel_base += a;
                    this.ptr += 2;
                    break;

                // Halt
                case 99:
                    this.active = false;
                    if (this.debug) { console.log(`EXT(${this.intcode[ptr]})`); }
                    return;

                default:
                    console.log(`ERROR: Unknown opcode: ${opcode}, instruction: ${instr}, at: ${this.ptr}`);
                    this.active = false;
                    return;
            }
        }
    }

}

module.exports = {IntcodeComputer}