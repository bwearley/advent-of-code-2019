'use strict';
const fs = require('fs');
const chalk = require('chalk');
const mathjs = require('mathjs');

var args = process.argv.slice(2);

class Moon {
    constructor(s) {
        var nums = s.match(/(\-?\d+)/g).map(n => Number(n));
        this.pos = [nums[0],nums[1],nums[2]];
        this.vel = [0,0,0];
        this.pos0 = [nums[0],nums[1],nums[2]];
    }
    move() {
        for (var i = 0; i < 3; i++) {
            this.pos[i] += this.vel[i];
        }
    }
    pe() {
        return Math.abs(this.pos[0]) + Math.abs(this.pos[1]) + Math.abs(this.pos[2]);
    }
    ke() {
        return Math.abs(this.vel[0]) + Math.abs(this.vel[1]) + Math.abs(this.vel[2]);
    }
    energy() {
        return (this.pe() * this.ke());
    }
    print() {
        console.log(`pos=<x=${this.pos[0]}, y=${this.pos[1]}, z=${this.pos[2]}>, vel=<x=${this.vel[0]}, y=${this.vel[1]}, z=${this.vel[2]}>`);
    }
}

function day12(filename) {

    const NUM_STEPS = 1000;

    // Load input
    const input = fs.readFileSync(filename)
        .toString()
        .split("\n")
        .filter(s => s.length != 0);

    // Init moons
    var moons = input.map(l => new Moon(l));
    var part1 = 0;

    var periods = [0,0,0];
   
    // Simulation loop
    for (var step = 0; ; step++) {

        // Debug
        /*
        console.log(`After ${step} steps:`);
        for (var m = 0; m < moons.length; m++) {
            moons[m].print();
        }
        var te = moons.map(m => m.energy()).reduce((a,b) => a+b,0);
        console.log(`Total energy: ${te}`);
        console.log();
        */
        if (step == NUM_STEPS) part1 = moons.map(m => m.energy()).reduce((a,b) => a+b,0);

        // Update moon velocities
        for (var m = 0; m < moons.length; m++) {
            var others = moons.filter((mn,ix) => ix != m);
            for (const o of others) {
                for (var i = 0; i < 3; i++) {
                    var dv = 0;
                    // Loop x,y,z
                    if (o.pos[i] > moons[m].pos[i]) {
                        dv = +1;
                    } else if (o.pos[i] < moons[m].pos[i]) {
                        dv = -1;
                    } else {
                        dv = 0;
                    }
                    moons[m].vel[i] += dv;
                }
            }
        }

        // Update moon positions
        moons.map(mn => mn.move());

        // Check if each axis matches initial state
        // (x,y,z axis motions are completely independent)
        for (var i = 0; i < 3; i++) {
            if (periods[i] == 0 &&
                moons.filter(mn => mn.vel[i] == 0).length == moons.length &&
                moons.filter(mn => mn.pos[i] == mn.pos0[i]).length == moons.length) periods[i] = step+1;
        }
        if (periods.filter(x => x != 0).length == 3) break;
    }

    const part2 = mathjs.lcm(...periods);

    return [part1,part2];
}

const [part1,part2] = day12(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 8625
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 332477126821644