use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fmt;

use intcode::intcode::IntcodeComputer;
extern crate intcode;

#[derive(Copy, Clone, PartialEq)]
pub enum ASCII {
    Newline  = '\n' as isize,
    Scaffold = '#' as isize,
    Open     = '.' as isize,
    RobotU   = '^' as isize,
    RobotD   = 'v' as isize,
    RobotL   = '<' as isize,
    RobotR   = '>' as isize,
}
impl fmt::Display for ASCII {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f,"{}",((*self as u8) as char).to_string())
    }
}
impl From<i64> for ASCII {
    fn from(value: i64) -> ASCII {
        match (value as u8) as char {
            '\n' => ASCII::Newline,
            '#'  => ASCII::Scaffold,
            '.'  => ASCII::Open,
            '^'  => ASCII::RobotU, 
            'v'  => ASCII::RobotD,
            '<'  => ASCII::RobotL,
            '>'  => ASCII::RobotR,
            _ => panic!("Unknown value: {}", value),
        }
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day17(&filename).unwrap();
}

fn day17(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let input = input.concat();
    let intcode = input
        .split(',')
        .map(|x| x.parse::<i64>().unwrap())
        .collect::<Vec<i64>>();

    // Initialize & Configure Base VM
    let mut vm = IntcodeComputer::new(intcode);
    vm.break_outputs = None;

    // Part 1
    const DRAW_INITIAL_MAP: bool = false;
    if DRAW_INITIAL_MAP { println!("Initial Map:"); }
    vm.run();
    let mut img: HashMap<[i64;2],ASCII> = HashMap::new();
    let outputs: Vec<i64> = vm.output_stack.iter().copied().collect();
    let mut x: i64 = 0; // +ve x to the right
    let mut y: i64 = 0; // +ve y down
    for out in &outputs {
        let o = ASCII::from(*out);
        match o {
            ASCII::Newline =>  { x = 0 ; y += 1; },
            _ => { img.insert([x,y],o); x += 1; },
        }
        if DRAW_INITIAL_MAP { print!("{}", o); }
    }

    // Get intersections
    let mut intersections: HashSet<[i64;2]> = HashSet::new();
    for (k,v) in img.iter() {
        let (x,y) = (k[0],k[1]);
        let up    = img.get(&[x  ,y+1]).unwrap_or(&ASCII::Open);
        let down  = img.get(&[x  ,y-1]).unwrap_or(&ASCII::Open);
        let left  = img.get(&[x-1,y  ]).unwrap_or(&ASCII::Open);
        let right = img.get(&[x+1,y  ]).unwrap_or(&ASCII::Open);
        if v      == &ASCII::Scaffold &&
            up    == &ASCII::Scaffold &&
            down  == &ASCII::Scaffold &&
            left  == &ASCII::Scaffold &&
            right == &ASCII::Scaffold {
                intersections.insert(*k);
        }
    }

    // Calculate Part 1
    let part1 = intersections.iter().map(|k| k[0]*k[1]).sum::<i64>();
    println!("Part 1: {}", part1); // 3936
    
    // Draw Map (w/ Intersections)
    const DRAW_INTERSECTIONS_MAP: bool = false;
    if DRAW_INTERSECTIONS_MAP {
        println!("Intersections Map:");
        let mut x: i64 = 0; // +ve x to the right
        let mut y: i64 = 0; // +ve y down
        for out in outputs {
            let o = ASCII::from(out);
            match ASCII::from(out) {
                ASCII::Newline =>  { x = 0 ; y += 1; println!(); },
                _ => {
                    match intersections.get(&[x,y]) {
                        Some(_) => print!("O"),
                        None => print!("{}",o),
                    }
                    x += 1;
                },
            }
        }
    }

    // R,4,L,12,L,8,R,4,L,8,R,10,R,10,R,6,R,4,L,12,L,8,R,4,R,4,R,10,L,12,R,4,L,12,L,8,R,4,L,8,R,10,R,10,R,6,R,4,L,12,L,8,R,4,R,4,R,10,L,12,L,8,R,10,R,10,R,6,R,4,R,10,L,12
    //  A = R,4,L,12,L,8,R,4,
    //  B = L,8,R,10,R,10,R,6,
    //  A = R,4,L,12,L,8,R,4,
    //  C = R,4,R,10,L,12,
    //  A = R,4,L,12,L,8,R,4,
    //  B = L,8,R,10,R,10,R,6,
    //  A = R,4,L,12,L,8,R,4,
    //  C = R,4,R,10,L,12,
    //  B = L,8,R,10,R,10,R,6,
    //  C = R,4,R,10,L,12

    // Part 2
    vm.reset();
    vm.set_addr(0,2);
    vm.push_ascii_line("A,B,A,C,A,B,A,C,B,C"); // MAIN
    vm.push_ascii_line("R,4,L,12,L,8,R,4");    // FUNCTION_A
    vm.push_ascii_line("L,8,R,10,R,10,R,6");   // FUNCTION_B
    vm.push_ascii_line("R,4,R,10,L,12");       // FUNCTION_C
    vm.push_ascii_line("n");
    vm.run();

    const LIVE_CAMERA: bool = true;
    let part2 = &vm.output_stack.back().unwrap_or(&0);
    
    if LIVE_CAMERA { vm.stream_ascii(); println!(); }
    else { println!("Part 2: {}", part2);  } // 785733

    Ok(())
}