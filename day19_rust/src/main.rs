use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::collections::HashMap;

use intcode::intcode::IntcodeComputer;
extern crate intcode;

fn check(vm: &mut IntcodeComputer, x: i64, y: i64) -> bool {
    vm.reset();
    vm.push_input(x);
    vm.push_input(y);
    vm.run();
    vm.pop_output() == 1
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day19(&filename).unwrap();
}

fn day19(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    const MAX_XDIM: i64 = 50;
    const MAX_YDIM: i64 = 50;
    const BOX: i64 = 99;

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let input = input.concat();
    let intcode = input
        .split(',')
        .map(|x| x.parse::<i64>().unwrap())
        .collect::<Vec<i64>>();

    // Initialize & Configure Base VM
    let mut vm = IntcodeComputer::new(intcode);

    // Part 1
    let mut img: HashMap<(i64,i64),i64> = HashMap::new();
    for y in 0..MAX_YDIM {
        for x in 0..MAX_XDIM {
            vm.reset();
            vm.push_input(x);
            vm.push_input(y);
            vm.run();
            let s = vm.pop_output();
            img.insert((x,y),s);
        }
    }
    let part1 = img.iter().filter(|(_,v)| *v == &1i64).count();
    println!("Part 1: {}", part1); // 199

    // Draw Map
    const DRAW_IMG: bool = false;
    if DRAW_IMG {
        for y in 0..MAX_YDIM {
            for x in 0..MAX_XDIM {
                match img.get(&(x,y)) {
                    Some(1) => print!("#"),
                    _       => print!("."),
                }
            }
            println!();
        }
    }

    // Part 2
    let mut beam_lower = (3,2); // starting point of lower edge of beam
    let part2: i64;
    loop {
        let (x0,y0) = beam_lower;
        if check(&mut vm, x0, y0-BOX) && check(&mut vm, x0+BOX,y0-BOX) {
            part2 = x0 * 10_000 + y0-BOX;
            break;
        }
        if check(&mut vm, x0+1, y0+1) {
            beam_lower = (x0+1,y0+1);
        } else {
            beam_lower = (x0+2,y0+1);
        }
    }
    println!("Part 2: {}", part2); // 10180726

    
    Ok(())
}