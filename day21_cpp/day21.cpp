#include <iostream>
#include <fstream>
#include <vector>
#include <map>

#include "../shared_cpp/stringtools.cpp"
#include "../intcode_cpp/intcode.cpp"

void Day21(std::string file) {

    std::ifstream in(file);
    std::vector<int64_t> input;
    // Load file data
    while (!in.eof()) {
        std::string line;
        std::getline(in, line);
        auto numStr = split(line,',');
        for (auto & num : numStr) {
            input.push_back(std::stol(num));
        }
    }

    // Initialize & Configure VM
    auto vm = IntcodeComputer(input);
    vm.break_on_out = false;

    // Part 1: 19358262
    vm.push_ascii_line("OR A J");
    vm.push_ascii_line("AND B J");
    vm.push_ascii_line("AND C J");
    vm.push_ascii_line("NOT J J");
    vm.push_ascii_line("AND D J");
    vm.push_ascii_line("WALK");
    vm.run();
    vm.stream_ascii();
    std::cout << std::endl;

    // Part 2: 1142686742
    vm.reset();
    vm.push_ascii_line("OR A J");
    vm.push_ascii_line("AND B J");
    vm.push_ascii_line("AND C J");
    vm.push_ascii_line("NOT J J");
    vm.push_ascii_line("AND D J");
    vm.push_ascii_line("OR E T");
    vm.push_ascii_line("OR H T");
    vm.push_ascii_line("AND T J");
    vm.push_ascii_line("RUN");
    vm.run();
    vm.stream_ascii();
    std::cout << std::endl;

}

int main(int argc, char** argv) {
    // Check for input file
    if (argc != 2) {
        printf("Must specify file at command line.\n");
        return -1;
    }

    Day21(argv[1]);
    return 0;
}