#include <iostream>
#include <fstream>
#include <array>
#include <vector>
#include <regex>

#include "../shared_cpp/stringtools.cpp"

auto get_digits(int64_t n) {
    std::vector<int64_t> digits;
    int64_t a = n;
    while (a > 0) {
        digits.push_back(a % 10);
        a /= 10;
    }
    std::reverse(digits.begin(), digits.end());
    return digits;
}

bool pair_check2(int64_t pw) {
    std::regex re("(1+|2+|3+|4+|5+|6+|7+|8+|9+)");
    auto s = std::to_string(pw);
    for (std::sregex_iterator i = std::sregex_iterator(s.begin(), s.end(), re);
        i != std::sregex_iterator();
        ++i) {
        if (i->length() == 2) return true;
    }
    return false;
}

int64_t validate_part1(int64_t pw) {
    auto digits = get_digits(pw);

    // Check never decreases
    for (auto i = 1; i != digits.size(); i++) {
        if (digits[i] < digits[i-1]) return false;
    }

    // Check for existence of pair
    for (auto i = 1; i != digits.size(); i++) {
        if (digits[i] == digits[i-1]) return true;
    }

    return false;
}

int64_t validate_part2(int64_t pw) {
    auto digits = get_digits(pw);

    // Check never decreases
    for (auto i = 1; i != digits.size(); i++) {
        if (digits[i] < digits[i-1]) return false;
    }

    // Check for existence of pair
    if (pair_check2(pw)) return true;
    return false;
}

void Day04(std::string file) {
    std::ifstream in(file);
    int64_t start, end;
    // Load file data
    while (!in.eof()) {
        std::string line;
        std::getline(in, line);
        auto parts = split(line,'-');
        start = std::stoi(parts[0]);
        end   = std::stoi(parts[1]);
    }

    int64_t part1 = 0;
    int64_t part2 = 0;
    for (auto pw = start; pw <= end; pw++) {
        if (validate_part1(pw)) part1 += 1;
        if (validate_part2(pw)) part2 += 1;
    }

    printf("Part 1: %lld\n", part1); // 1855
    printf("Part 2: %lld\n", part2); // 1253
}

int main(int argc, char** argv) {
    // Check for input file
    if (argc != 2) {
        printf("Must specify file at command line.\n");
        return -1;
    }

    Day04(argv[1]);
    return 0;
}