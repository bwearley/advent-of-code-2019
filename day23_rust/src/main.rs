use std::env;
use std::io::{self, prelude::*, BufReader};
use std::collections::HashSet;
use std::fs::File;

use intcode::intcode::IntcodeComputer;
extern crate intcode;

fn day23(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    const NUM_COMPUTERS: usize = 50;

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let input = input.concat();
    let intcode = input
        .split(',')
        .map(|x| x.parse::<i64>().unwrap())
        .collect::<Vec<i64>>();

    // Initailize computers
    let mut comps: Vec<IntcodeComputer> = 
        (0..NUM_COMPUTERS)
        .map(|_| IntcodeComputer::new(intcode.clone()))
        .collect();

    for cix in 0..NUM_COMPUTERS {
        comps[cix].default_input = Some(-1);
        comps[cix].break_outputs = Some(3);
        comps[cix].push_input(cix as i64); // Assign network address
    }

    let mut nat = (0,0);

    // Process
    let mut seen: HashSet<i64> = HashSet::new();
    let mut part1: i64 = 0;
    let part2: i64;
    'main_lp: loop {
        for cix in 0..NUM_COMPUTERS {
            comps[cix].run();
            if comps[cix].output_stack.is_empty() { continue; }
            
            let outputs: Vec<i64> = comps[cix].output_stack.iter().copied().collect();
            comps[cix].output_stack.clear();
            for batch in outputs.chunks(3) {
                let ip = batch[0];
                let x  = batch[1];
                let y  = batch[2];
                match ip {
                    255 => {
                        if part1 == 0 { part1 = y; }
                        nat = (x,y);
                    }
                    _ => {
                        comps[ip as usize].push_input(x);
                        comps[ip as usize].push_input(y);
                    }
                }
            }
        }

        if comps.iter().filter(|c| c.input_stack.len() <= 1).count() == NUM_COMPUTERS {
            comps[0].push_input(nat.0);
            comps[0].push_input(nat.1);
            match seen.get(&nat.1) {
                Some(_) => { part2 = nat.1; break 'main_lp; },
                _ => { seen.insert(nat.1); },
            }
        }
    }

    println!("Part 1: {}", part1); // 21089
    println!("Part 2: {}", part2); // 16658
    
    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day23(&filename).unwrap();
}