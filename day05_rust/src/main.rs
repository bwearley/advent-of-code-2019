use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

use intcode::intcode::IntcodeComputer;
extern crate intcode;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day05(&filename).unwrap();
}

fn day05(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let input = input.concat();
    let intcode = input
        .split(',')
        .map(|x| x.parse::<i64>().unwrap())
        .collect::<Vec<i64>>();

    let mut vm = IntcodeComputer::new(intcode);

    // Part 1
    vm.push_input(1);
    vm.break_outputs = None;
    vm.run();
    let part1 = vm.last_output();

    // Part 2
    vm.reset();
    vm.push_input(5);
    vm.run();
    let part2 = vm.pop_output();

    // Output
    println!("Part 1: {}", part1); // 7265618
    println!("Part 2: {}", part2); // 7731427

    Ok(())
}