use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

use intcode::intcode::IntcodeComputer;
extern crate intcode;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day21(&filename).unwrap();
}

fn day21(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let input = input.concat();
    let intcode = input
        .split(',')
        .map(|x| x.parse::<i64>().unwrap())
        .collect::<Vec<i64>>();

    // Initialize & Configure Base VM
    let mut vm = IntcodeComputer::new(intcode);
    vm.break_outputs = None;

    // Part 1: 19358262
    vm.push_ascii_line("OR A J");
    vm.push_ascii_line("AND B J");
    vm.push_ascii_line("AND C J");
    vm.push_ascii_line("NOT J J");
    vm.push_ascii_line("AND D J");
    vm.push_ascii_line("WALK");
    vm.run();
    vm.stream_ascii();
    println!();

    // Part 2: 1142686742
    vm.reset();
    vm.push_ascii_line("OR A J");
    vm.push_ascii_line("AND B J");
    vm.push_ascii_line("AND C J");
    vm.push_ascii_line("NOT J J");
    vm.push_ascii_line("AND D J");
    vm.push_ascii_line("OR E T");
    vm.push_ascii_line("OR H T");
    vm.push_ascii_line("AND T J");
    vm.push_ascii_line("RUN");
    vm.run();
    vm.stream_ascii();
    println!();
    
    Ok(())
}