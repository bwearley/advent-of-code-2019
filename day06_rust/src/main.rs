use std::env;

struct Orbit {
    root: String,
    orbit: String,
}
impl Orbit {
    pub fn new(input: &str) -> Orbit {
        let parts: Vec<_> = input.split(')').collect();
        Orbit {
            root: parts[0].to_string(),
            orbit: parts[1].to_string(),
        }
    }
}

fn num_orbits(orbits: &[Orbit], start: &str, num: u64) -> u64 {
    let num_in = num + 1;
    num + orbits.iter()
        .filter(|o| o.root == start)
        .map(|o| num_orbits(&orbits, &o.orbit, num_in))
        .sum::<u64>()
}

fn transfers(orbits: &[Orbit], start: &str) -> Vec<String> {
    let mut path: Vec<_> = Vec::new();
    path.push(start.to_string());
    if start == "COM" { return path; }
    let new_path = orbits.iter().filter(|o| o.orbit == start).flat_map(|o| transfers(&orbits, &o.root));
    for p in new_path {
        path.push(p);
    }
    path
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day06(&filename);
}

fn day06(input: &str) {
    let input = std::fs::read_to_string(input).expect("Error reading input file.");
    let input: Vec<_> = input.split('\n').collect();

    let orbits: Vec<Orbit> = input.iter().map(|x| Orbit::new(x)).collect();

    // Part 1
    let part1 = num_orbits(&orbits, "COM", 0);
    println!("Part 1: {}", part1); // 333679

    // Part 2
    // Calculates the paths from each back to COM,
    // then subtracts the parts of each path in common
    // to get just the path between each. Subtracts 2
    // to eliminate YOU and SAN.
    let part2_1 = transfers(&orbits, "YOU");
    let part2_2 = transfers(&orbits, "SAN");
    let part2 = part2_1.iter().filter(|t1| part2_2.iter().filter(|t2| t1==t2).count() == 0).count()
              + part2_2.iter().filter(|t2| part2_1.iter().filter(|t1| t1==t2).count() == 0).count()
              - 2;
    println!("Part 2: {}", part2); // 370

}