use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

extern crate fancy_regex;
use fancy_regex::Regex;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day04(&filename).unwrap();
}

fn validate_part1(input: u64) -> bool {
    let digits: Vec<_> = input.to_string().chars().map(|n| n.to_digit(10).unwrap()).collect();

    // Check never decreases
    for pair in digits.windows(2) {
        if pair[0] > pair[1] { return false; }
    }

    // Check for existence of pair
    for pair in digits.windows(2) {
        if pair[0] == pair[1] { return true; }
    }

    false
}

fn validate_part2(input: u64) -> bool {
    let digits: Vec<_> = input.to_string().chars().map(|n| n.to_digit(10).unwrap()).collect();

    // Check never decreases
    for pair in digits.windows(2) {
        if pair[0] > pair[1] { return false; }
    }

    // Check for existence of pair
    let re = Regex::new(r"(?:^|(.)(?!\1))(\d)\2(?!\2)").unwrap();
    if re.is_match(&input.to_string()).unwrap() { return true; }
    false
}

fn day04(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: String = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    let start_end: Vec<_> = input.split("-").map(|n| n.parse::<u64>().unwrap()).collect();
    let start = start_end[0];
    let end = start_end[1];

    let part1 = (start..=end).filter(|pw| validate_part1(*pw)).count();
    println!("Part 1: {}", part1); // 1855

    let part2 = (start..=end).filter(|pw| validate_part2(*pw)).count();
    println!("Part 2: {}", part2); // 1253

    Ok(())
}