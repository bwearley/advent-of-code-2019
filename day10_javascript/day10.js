'use strict';
const fs = require('fs');
const chalk = require('chalk');

var args = process.argv.slice(2);

function onlyUnique(value, index, self) { 
    return self.indexOf(value) === index;
}

class Asteroid {
    constructor(x,y) {
        this.x = x;
        this.y = y;
        this.angle = 0;
        this.dist = 0;
        this.destroyed = false;
    }
    destroy () { this.destroyed = true; }
    setAngleDistanceFrom(x,y) {
        this.dist = Math.sqrt(Math.pow(this.x-x,2) + Math.pow(this.y-y,2));
        var a = Math.atan2(y-this.y,x-this.x) - Math.PI/2;
        this.angle = a >= 0 ? a : 2*Math.PI + a;
    }
}

function day10(filename) {

    // Load input
    const input = fs.readFileSync(filename)
        .toString()
        .split("\n")
        .filter(s => s.length > 0);

    var asteroids = []
    input.map((line,y) => {
        line.split("")
            .map((ch,x) => {
                if (ch == "#") asteroids.push(new Asteroid(x,y));
            })
    });

    // Part 1
    var part1 = 0;
    var best = [0,0];
    for (const asteroid of asteroids) {
        const slopes = asteroids
            .map(a => Math.atan2((a.y-asteroid.y),(a.x-asteroid.x)) )
            .filter(onlyUnique)
        if (slopes.length > part1) best = [asteroid.x,asteroid.y]
        part1 = Math.max(part1, slopes.length);
    }
    console.log(`Best: (${best[0]},${best[1]})`);

    // Part 2
    asteroids.map(a => a.setAngleDistanceFrom(best[0],best[1]));
    // Sort first on angle, then on distance
    asteroids.sort(
        (a,b) => {
        if (a.angle == b.angle) {
            return a.dist - b.dist;
        } else {
            return a.angle - b.angle;
        }
    });

    var lastDestroyedAngle = null;
    var numDestroyed = 0;
    var part2 = 0;
    var done = false;
    while (!done) {
        for (const a of asteroids) {
            if (a.destroyed || a.angle == lastDestroyedAngle) continue;
            lastDestroyedAngle = a.angle;
            a.destroy();
            numDestroyed += 1;
            //console.log(`Destroyed (${numDestroyed}): (${a.x},${a.y})`)
            if (numDestroyed == 200) {
                part2 = 100*a.x + a.y;
                done=true;
                break;
            }
        }
        lastDestroyedAngle = null;
    }
    return [part1,part2];
}
  
const [part1,part2] = day10(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 282
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 1008