'use strict';
const fs = require('fs');
const chalk = require('chalk');
const mathjs = require('mathjs');
const Hashmap = require('hashmap');

var {IntcodeComputer} = require('../intcode_js/intcode.js');

var args = process.argv.slice(2); 

function part1(filename) {

    // Load input
    const input = fs.readFileSync(filename)
        .toString()
        .split(",")
        .map(n => Number(n));
    
    const turn_left = mathjs.complex(0, 1);
    const turn_right = mathjs.complex(0, -1);

    // Define colors
    const BLACK = 0;
    const WHITE = 1;

    // Define turns
    const LEFT = 0;
    const RIGHT = 1;

    // Initialize & Configure VM
    var vm = new IntcodeComputer(input);
    vm.break_on_out = true;
    //vm.debug = true;
    vm.push_input(BLACK);

    // Initialize Map State
    var img = new Hashmap();
    var pos = mathjs.complex(0, 0);
    var dir = mathjs.complex(0, -1);
    img.set([pos.re,pos.im],BLACK);

    // Run robot
    while (true) {
        vm.run();
        if (!vm.active) { break; }
        let color = vm.pop_output();
        vm.run();
        if (!vm.active) { break; }
        let turn = vm.pop_output();
        switch (turn) {
            case LEFT:
                dir = mathjs.multiply(dir, turn_left);
                break;
            case RIGHT:
                dir = mathjs.multiply(dir, turn_right);
                break;
            default:
                console.log(`Unknown direction: ${turn}`);
                return;
        }
        img.set([pos.re,pos.im],color);
  
        // Move
        pos = mathjs.add(pos,dir);

        // Start next
        var next = img.get([pos.re,pos.im]);
        next = next === undefined ? BLACK : next;
        vm.push_input(next);
    }
    
    return img.keys().length;
}

function part2(filename) {

    // Load input
    const input = fs.readFileSync(filename)
        .toString()
        .split(",")
        .map(n => Number(n));
    
    const turn_left = mathjs.complex(0, 1);
    const turn_right = mathjs.complex(0, -1);

    // Define colors
    const BLACK = 0;
    const WHITE = 1;

    // Define turns
    const LEFT = 0;
    const RIGHT = 1;

    // Initialize & Configure VM
    var vm = new IntcodeComputer(input);
    vm.break_on_out = true;
    //vm.debug = true;
    vm.push_input(WHITE);

    // Initialize Map State
    var img = new Hashmap();
    var pos = mathjs.complex(0, 0);
    var dir = mathjs.complex(0, -1);
    img.set([pos.re,pos.im],WHITE);

    // Run robot
    while (true) {
        vm.run();
        if (!vm.active) { break; }
        let color = vm.pop_output();
        vm.run();
        if (!vm.active) { break; }
        let turn = vm.pop_output();
        switch (turn) {
            case LEFT:
                dir = mathjs.multiply(dir, turn_left);
                break;
            case RIGHT:
                dir = mathjs.multiply(dir, turn_right);
                break;
            default:
                console.log(`Unknown direction: ${turn}`);
                return;
        }
        img.set([pos.re,pos.im],color);
  
        // Move
        pos = mathjs.add(pos,dir);

        // Start next
        var next = img.get([pos.re,pos.im]);
        next = next === undefined ? BLACK : next;
        vm.push_input(next);
    }

    // Get image limits
    const xmax = Math.max( ...img.keys().map(k => k[0]) );
    const xmin = Math.min( ...img.keys().map(k => k[0]) ); 
    const ymax = Math.max( ...img.keys().map(k => k[1]) );
    const ymin = Math.min( ...img.keys().map(k => k[1]) );

    // Draw
    console.log(chalk.blue("Part 2:"));
    for (var y = ymin; y <= ymax; y++) {
        for (var x = xmax; x >= xmin; x--) {
            switch (img.get([x,y])) {
                case BLACK:
                    process.stdout.write(" ");
                    break;
                case WHITE:
                    process.stdout.write("#");
                    break;
                default:
                    process.stdout.write(" ");
                    break;
            }
        }
        console.log();
    }
}

console.log(chalk.blue("Part 1: ") + chalk.yellow(part1(args[0]))); // 2184
part2(args[0]); // AHCHZEPK
