'use strict';
const fs = require('fs');
const chalk = require('chalk');
const Hashmap = require('hashmap');

var {IntcodeComputer} = require('../intcode_js/intcode.js');

var args = process.argv.slice(2); 

function chunk(array, size) {
    const chunked_arr = [];
    for (let i = 0; i < array.length; i++) {
      const last = chunked_arr[chunked_arr.length - 1];
      if (!last || last.length === size) {
        chunked_arr.push([array[i]]);
      } else {
        last.push(array[i]);
      }
    }
    return chunked_arr;
}

function day23(filename) {

    // Load input
    const input = fs.readFileSync(filename)
        .toString()
        .split(",")
        .map(n => Number(n));

    const NUM_COMPS = 50;
    var comps = [];
    for (var i = 0; i < NUM_COMPS; i++) {
        comps.push(new IntcodeComputer(input.slice()));
        comps[i].neg_1_on_empty_in = true;
        comps[i].break_on_out = false;
        comps[i].break_on_out3 = true;
        comps[i].push_input(i); // Assign network address
    }

    // Part 2
    var NAT = [0,0];
    var seen = new Hashmap();

    // Process
    var part1 = 0;
    var part2 = 0;
    var done = false;
    while (!done) {

        for (var i = 0; i < NUM_COMPS; i++) {
            comps[i].run();

            if (comps[i].output_stack.length == 0) continue;
            var outputs = [];
            while (comps[i].output_stack.length != 0) {
                outputs.push(comps[i].output_stack.first());
                comps[i].output_stack.shift();
            }

            const batches = chunk(outputs,3);
            for (const chnk of batches) {
                var ip = chnk[0];
                var x  = chnk[1];
                var y  = chnk[2];
                switch (ip) {
                    case 255:
                        if (part1 == 0) part1 = y;
                        NAT[0] = x;
                        NAT[1] = y;
                        break;
                    default:
                        comps[ip].push_input(x);
                        comps[ip].push_input(y);
                        break;
                }
            }
        }

        if (comps.filter(c => c.input_stack.length == 1).length == NUM_COMPS) {
            comps[0].push_input(NAT[0]);
            comps[0].push_input(NAT[1]);
            if (seen.has(NAT[1])) {
                part2 = NAT[1];
                done = true;
            }
            seen.set(NAT[1],1);
        }
    }
    return [part1,part2];
}

const [part1,part2] = day23(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 21089
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 16658