use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::collections::HashMap;

use intcode::intcode::IntcodeComputer;
extern crate intcode;

extern crate point2d;
use point2d::point2d::Point2D;

const DRAW_MAP: bool = false;

#[derive(PartialEq)]
pub enum MapTile {
    Empty = 0,
    Wall = 1,
    Block = 2,
    Paddle = 3,
    Ball = 4,
    Score = 10,
}

fn day13(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let input = input.concat();
    let intcode = input
        .split(',')
        .map(|x| x.parse::<i64>().unwrap())
        .collect::<Vec<i64>>();

    // Initialize & Configure VM
    let mut vm = IntcodeComputer::new(intcode);
    vm.break_outputs = None;
    vm.set_addr(0,2); // insert quarters

    // Init Game
    let mut part1 = 0;
    let mut img: HashMap<Point2D,MapTile> = HashMap::new();
    let mut score = 0;
    let score_point = Point2D::new(-1,0);

    // Run Game
    while vm.active {
        vm.run();
        let outputs = vm.output_stack.iter().map(|x| *x).collect::<Vec<i64>>();
        for iter in outputs.chunks(3) {
            let pt = Point2D::new(iter[0],iter[1]);
            let d = match iter[2] {
                0 => MapTile::Empty,
                1 => MapTile::Wall,
                2 => MapTile::Block,
                3 => MapTile::Paddle,
                4 => MapTile::Ball,
                _ => MapTile::Score,
            };
            if pt == score_point {
                score = iter[2];
            } else {
                img.insert(pt,d);
            }
        }

        // Calculate Part 1
        if part1 == 0 { part1 = img.values().filter(|v| *v == &MapTile::Block).count(); }

        // Get image limits
        let xmax: i64 = img.keys().map(|pt| pt.x).max().unwrap();
        let xmin: i64 = img.keys().map(|pt| pt.x).min().unwrap(); 
        let ymax: i64 = img.keys().map(|pt| pt.y).max().unwrap();
        let ymin: i64 = img.keys().map(|pt| pt.y).min().unwrap();

        // Draw
        if DRAW_MAP {
            println!("Map:");
            println!("Score: {}", score);
            for y in ymin..=ymax {
                for x in xmin..=xmax {
                    match img.get(&Point2D::new(x,y)) {
                        Some(MapTile::Empty) => print!(" "),
                        Some(MapTile::Wall) => print!("#"),
                        Some(MapTile::Block) => print!("*"),
                        Some(MapTile::Paddle) => print!("="),
                        Some(MapTile::Ball) => print!("O"),
                        Some(MapTile::Score) => panic!("Error: Encountered Score while drawing map"),
                        None => print!(" "),
                    }   
                }
                println!();
            }
        }

        // Get ball & paddle positions
        let ballx: i64   = img.iter().filter(|(_,v)| *v == &MapTile::Ball  ).map(|(pt,_)| pt.x).next().unwrap();
        let paddlex: i64 = img.iter().filter(|(_,v)| *v == &MapTile::Paddle).map(|(pt,_)| pt.x).next().unwrap();

        // Submit joystick input
        vm.push_input((ballx-paddlex).signum())
    }

    println!("Part 1: {}", part1); // 329
    println!("Part 2: {}", score); // 15973
    
    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day13(&filename).unwrap();
}