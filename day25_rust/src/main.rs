use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

extern crate itertools;
use itertools::Itertools;

use intcode::intcode::IntcodeComputer;
extern crate intcode;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day25(&filename).unwrap();
}

fn day25(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let input = input.concat();
    let intcode = input
        .split(',')
        .map(|x| x.parse::<i64>().unwrap())
        .collect::<Vec<i64>>();

    // Initialize & Configure VM
    let mut vm = IntcodeComputer::new(intcode);
    vm.break_outputs = None;

    // Part 1
    vm.push_ascii_line("east");
    vm.push_ascii_line("north");
    vm.push_ascii_line("north");
    vm.push_ascii_line("take spool of cat6");
    vm.push_ascii_line("south");
    vm.push_ascii_line("east");
    vm.push_ascii_line("take mug");
    vm.push_ascii_line("north");
    vm.push_ascii_line("north");
    vm.push_ascii_line("west");
    vm.push_ascii_line("take asterisk");
    vm.push_ascii_line("south");
    vm.push_ascii_line("take monolith");
    vm.push_ascii_line("north");
    vm.push_ascii_line("east");
    vm.push_ascii_line("south");
    vm.push_ascii_line("east");
    vm.push_ascii_line("take sand");
    vm.push_ascii_line("south");
    vm.push_ascii_line("west");
    vm.push_ascii_line("take prime number");
    vm.push_ascii_line("east");
    vm.push_ascii_line("north");
    vm.push_ascii_line("east");
    vm.push_ascii_line("north");
    vm.push_ascii_line("south");
    vm.push_ascii_line("south");
    vm.push_ascii_line("take tambourine");
    vm.push_ascii_line("west");
    vm.push_ascii_line("take festive hat");
    vm.push_ascii_line("north");
    vm.push_ascii_line("inv");

    let items = "prime number,spool of cat6,festive hat,monolith,mug,asterisk,sand,tambourine";
    let items: Vec<_> = items.split(",").collect();
    'outer_lp: for num_items in 3..8 {
        for inventory in items.iter().combinations(num_items) {
            for item in &items {
                vm.push_ascii_line(format!("drop {}",item).as_ref());
            }
            for item in inventory {
                vm.push_ascii_line(format!("take {}",item).as_ref());
            }
            vm.push_ascii_line("west");
            vm.run();
            if !vm.active { break 'outer_lp; }
        }
    }

    vm.stream_ascii(); // 2228740
    
    Ok(())
}