'use strict';

const fs = require('fs');
const chalk = require('chalk');
const Hashmap = require('hashmap');

var args = process.argv.slice(2);

const DIM = 5;

function getNeighbors(x,y) {
    var neighbors = [];
    if ((x-0 >=  0) && (y-1 >=  0)) { neighbors.push( [x-0,y-1] ); }
    if ((x-1 >=  0) && (y-0 >=  0)) { neighbors.push( [x-1,y-0] ); }
    if ((x+1 < DIM) && (y-0 >=  0)) { neighbors.push( [x+1,y-0] ); }
    if ((x-0 >=  0) && (y+1 < DIM)) { neighbors.push( [x-0,y+1] ); }
    return neighbors;
}

function getRecursiveNeighbors(x,y,d) {
    var neighbors = [];
    if (x == 0) neighbors.push( [1,2,d-1] ); // cell 12
    if (x == 4) neighbors.push( [3,2,d-1] ); // cell 14
    if (y == 0) neighbors.push( [2,1,d-1] ); // cell 8
    if (y == 4) neighbors.push( [2,3,d-1] ); // cell 18

    if (x == 2 && y == 1) {
        // cells ABCDE of d+1
        neighbors.push( [0,0,d+1] );
        neighbors.push( [1,0,d+1] );
        neighbors.push( [2,0,d+1] );
        neighbors.push( [3,0,d+1] );
        neighbors.push( [4,0,d+1] );
    }
    if (x == 1 && y == 2) {
        // cells AFKPU of d+1
        neighbors.push( [0,0,d+1] );
        neighbors.push( [0,1,d+1] );
        neighbors.push( [0,2,d+1] );
        neighbors.push( [0,3,d+1] );
        neighbors.push( [0,4,d+1] );
    }
    if (x == 3 && y == 2) {
        // cells EJOTY of d+1
        neighbors.push( [4,0,d+1] );
        neighbors.push( [4,1,d+1] );
        neighbors.push( [4,2,d+1] );
        neighbors.push( [4,3,d+1] );
        neighbors.push( [4,4,d+1] );
    }
    if (x == 2 && y == 3) {
        // cells UVWXY of d+1
        neighbors.push( [0,4,d+1] );
        neighbors.push( [1,4,d+1] );
        neighbors.push( [2,4,d+1] );
        neighbors.push( [3,4,d+1] );
        neighbors.push( [4,4,d+1] );
    }

    if ((x-0 >=  0) && (y-1 >=  0)) neighbors.push( [x-0,y-1,d] );
    if ((x-1 >=  0) && (y-0 >=  0)) neighbors.push( [x-1,y-0,d] );
    if ((x+1 < DIM) && (y-0 >=  0)) neighbors.push( [x+1,y-0,d] );
    if ((x-0 >=  0) && (y+1 < DIM)) neighbors.push( [x-0,y+1,d] );
    return neighbors;
}
/*
     |     |         |     |     
  1  |  2  |    3    |  4  |  5  
     |     |         |     |     
-----+-----+---------+-----+-----
     |     |         |     |     
  6  |  7  |    8    |  9  |  10 
     |     |         |     |     
-----+-----+---------+-----+-----
     |     |A|B|C|D|E|     |     
     |     |-+-+-+-+-|     |     
     |     |F|G|H|I|J|     |     
     |     |-+-+-+-+-|     |     
 11  | 12  |K|L|?|N|O|  14 |  15 
     |     |-+-+-+-+-|     |     
     |     |P|Q|R|S|T|     |     
     |     |-+-+-+-+-|     |     
     |     |U|V|W|X|Y|     |     
-----+-----+---------+-----+-----
     |     |         |     |     
 16  | 17  |    18   |  19 |  20 
     |     |         |     |     
-----+-----+---------+-----+-----
     |     |         |     |     
 21  | 22  |    23   |  24 |  25 
     |     |         |     |     

    Tile 19 has four adjacent tiles: 14, 18, 20, and 24.
    Tile G has four adjacent tiles: B, F, H, and L.
    Tile D has four adjacent tiles: 8, C, E, and I.
    Tile E has four adjacent tiles: 8, D, 14, and J.
    Tile 14 has eight adjacent tiles: 9, E, J, O, T, Y, 15, and 19.
    Tile N has eight adjacent tiles: I, O, S, and five tiles within the sub-grid marked ?.

*/

function drawMap(map) {
    for (var y = 0; y < DIM; y++) {
        for (var x = 0; x < DIM; x++) {
            if (map[x][y] == 1) { process.stdout.write("#"); }
            else { process.stdout.write("."); }
        }
        console.log();
    }
}

function biodiversity(map) {
    var bio = 0;
    var ix = 0;
    for (var y = 0; y < DIM; y++) {
        for (var x = 0; x < DIM; x++) {
            if (map[x][y] == 1) bio += Math.pow(2,ix);
            ix += 1;
        }
    }
    return bio;
}

function part1(filename) {

    // Init screen
    var pixels = new Array();
    for (var i = 0; i < DIM; i++) {
        pixels[i] = new Array(DIM).fill(0);
    }

    // Load input
    const input = fs.readFileSync(filename)
       .toString()
       .split("\n")
       .filter(s => s.length > 0);

    // Initialize state
    input.map((row,y) => {
        var chars = row.split("");
        chars.map((char,x) => {
            switch (char) {
                case "#":
                    pixels[x][y] = 1;
                    break;
                case ".":
                    pixels[x][y] = 0;
                    break;
                default:
                    console.log("ERROR: Unknown character: " + char);
                    break;
            }
        });
    });

    var seen = new Hashmap();
    //drawMap(pixels); console.log();

    // Run
    var part1 = 0;
    for (var step = 0; ; step++) {
        var current = JSON.parse(JSON.stringify(pixels));

        for (var y = 0; y < DIM; y++) {
            for (var x = 0; x < DIM; x++) {
                var neighbors = getNeighbors(x,y);

                // Count neighbors that are bugs
                var numBugs = 0;
                for (var n of neighbors) {
                    var [xn,yn] = n;
                    if (current[xn][yn] == 1) numBugs += 1;
                }
            
                // An empty space becomes infested with a bug if exactly one or two bugs are adjacent to it.
                if (current[x][y] == 0 && (numBugs == 1 || numBugs == 2)) {
                    pixels[x][y] = 1;
                }

                // A bug dies (becoming an empty space) unless there is exactly one bug adjacent to it.
                if (current[x][y] == 1 && !(numBugs == 1)) {
                    pixels[x][y] = 0;
                }
            }
        }
        // Check if previously seen
        if (seen.has(pixels)) {
            part1 = biodiversity(pixels);
            break;
        } else {
            seen.set(pixels,1);
        }

    }
    return part1;
}

function part2(filename) {

    const NUM_MINUTES = 200;
    const NUM_RECURSIVE_LAYERS = 250;

    // Init screen
    var pixels = new Array();
    for (var x = 0; x < DIM; x++) {
        pixels[x] = new Array();
        for (var y = 0; y < DIM; y++) {
            pixels[x][y] = new Array(NUM_RECURSIVE_LAYERS).fill(0);
        }
    }

    // Load input
    const input = fs.readFileSync(filename)
       .toString()
       .split("\n")
       .filter(s => s.length > 0);

    // Initialize state
    const BASE = 125;
    input.map((row,y) => {
        var chars = row.split("");
        chars.map((char,x) => {
            switch (char) {
                case "#":
                    pixels[x][y][BASE] = 1;
                    break;
                case ".":
                    pixels[x][y][BASE] = 0;
                    break;
                default:
                    console.log("ERROR: Unknown character: " + char);
                    break;
            }
        });
    });
    
    // Run
    for (var step = 0; step < NUM_MINUTES; step++) {
        var current = JSON.parse(JSON.stringify(pixels));

        for (var d = 1; d < NUM_RECURSIVE_LAYERS-1; d++) {
            for (var y = 0; y < DIM; y++) {
                for (var x = 0; x < DIM; x++) {
                    if (x == 2 && y == 2) continue; // middle (recursive) tile
                    var neighbors = getRecursiveNeighbors(x,y,d);

                    // Count neighbors that are bugs
                    var numBugs = 0;
                    for (var n of neighbors) {
                        var [xn,yn,dn] = n;
                        if (current[xn][yn][dn] == 1) numBugs += 1;
                    }
                
                    // An empty space becomes infested with a bug if exactly one or two bugs are adjacent to it.
                    if (current[x][y][d] == 0 && (numBugs == 1 || numBugs == 2)) {
                        pixels[x][y][d] = 1;
                    }

                    // A bug dies (becoming an empty space) unless there is exactly one bug adjacent to it.
                    if (current[x][y][d] == 1 && !(numBugs == 1)) {
                        pixels[x][y][d] = 0;
                    }
                }
            }
        }
    }
    // Calculate number of bugs
    var part2 = 0;
    for (var d = 0; d < NUM_RECURSIVE_LAYERS; d++) {
        for (var y = 0; y < DIM; y++) {
            for (var x = 0; x < DIM; x++) {
                if (pixels[x][y][d] == 1) part2 += 1;
            }
        }
    }
    return part2;
}

console.log(chalk.blue("Part 1: ") + chalk.yellow(part1(args[0]))); // 32506764
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2(args[0]))); // 1963