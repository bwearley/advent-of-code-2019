'use strict';
const fs = require('fs');
const chalk = require('chalk');
const Hashmap = require('hashmap');

var args = process.argv.slice(2);

class Ingredient {
    constructor(arr) {
        const [qty,name] = [arr[0], arr[1]];
        this.qty = Number(qty);
        this.name = name;
    }
}

class Recipe {
    constructor(s) {
        let re = /(\d+ \w+)+/g;
        let materials = s.match(re).map(p => {
            let parts = p.split(" ");
            return [Number(parts[0]),parts[1]];
        });
        let result = materials.pop();
        this.result = new Ingredient(result);
        this.ingredients = materials.map(m => new Ingredient(m))
    }
}

var recipes = []
var surplus = new Hashmap();

function requiredOreToMakeProduct(product, qty) {
    var recipe = recipes.find(r => r.result.name == product);
    var existing = surplus.has(product) ? surplus.get(product) : 0;
    var multiple = Math.ceil(Math.max((qty - existing),0) / recipe.result.qty);
    var extra = (recipe.result.qty * multiple) - (qty - existing);
    if (product != "ORE") { surplus.set(product,extra); }
    var ore = 0;
    for (const ingr of recipe.ingredients) {
        if (ingr.name == "ORE") {
            ore += multiple * ingr.qty;
        } else {
            ore += requiredOreToMakeProduct(ingr.name, multiple * ingr.qty);
        }
    }
    return ore;
}

function day14(filename) {

    const input = fs.readFileSync(filename)
        .toString()
        .split("\n")
        .filter(s => s.length > 0);

    recipes = input.map(r => new Recipe(r));

    // Part 1
    var part1 = requiredOreToMakeProduct("FUEL", 1);

    // Part 2
    var part2 = 0;
    const ORE_STORAGE = 1000000000000;
    for (var i = 11780000; ; i++) {
        surplus.clear();
        if (requiredOreToMakeProduct("FUEL", i) <= ORE_STORAGE) {
            part2 = i;
        } else {
            break;
        }
    }

    return [part1, part2];
}

const [part1,part2] = day14(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 216477
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 11788286