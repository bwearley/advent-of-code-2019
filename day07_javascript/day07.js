'use strict';
const fs = require('fs');
const chalk = require('chalk');

var {IntcodeComputer} = require('../intcode_js/intcode.js');

var args = process.argv.slice(2); 

const permutations = array => {
    if (array.length < 2) {
        // Base case, return single-element array wrapped in another array
        return [array];
    } else {
        let perms = [];
        for (let index = 0; index < array.length; index++) {
            // Make a fresh copy of the passed array and remove the current element from it
            let rest = array.slice();
            rest.splice(index, 1);
            
            // Call our function on that sub-array, storing the result: an array of arrays
            let ps = permutations(rest);
            
            // Add the current element to the beginning of each sub-array and add the new
            // permutation to the output array
            const current = [array[index]]
            for (let p of ps) {
                perms.push(current.concat(p));
            }
        }
        return perms;
    }
};

function day07(filename) {

    // Load input
    const input = fs.readFileSync(filename)
        .toString()
        .split(",")
        .map(n => Number(n));

    var amps = [
        new IntcodeComputer(input.slice()),
        new IntcodeComputer(input.slice()),
        new IntcodeComputer(input.slice()),
        new IntcodeComputer(input.slice()),
        new IntcodeComputer(input.slice()),
    ];

    // Part 1
    var settings1 = [...Array(5).keys()];
    var part1 = 0;
    for (const permute of permutations(settings1)) {
        // Reset each computer and set intial inputs
        for (var i = 0; i < amps.length; i++) {
            amps[i].reset();
            amps[i].push_input(permute[i]);
        }

        // Run each computer
        var last = 0;
        for (var i = 0; i < amps.length; i++) {
            amps[i].push_input(last);
            amps[i].run();
            last = amps[i].pop_output();

        }
        part1 = Math.max(part1, last);
    }

    // Part 2
    var settings2 = [5, 6, 7, 8, 9];
    var part2 = 0;
    for (const permute of permutations(settings2)) {
        // Reset each computer and set intial inputs
        for (var i = 0; i < amps.length; i++) {
            amps[i].reset();
            amps[i].push_input(permute[i]);
        }

        // Run each computer
        var last = 0;
        var done = false;
        while (!done) {
            for (var i = 0; i < amps.length; i++) {
                amps[i].push_input(last);
                amps[i].run();
                if (!amps[i].active) { done = true; break; }
                last = amps[i].pop_output();
            }
        }
        part2 = Math.max(part2, last);
    }
    return [part1,part2];
}

const [part1,part2] = day07(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 47064
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 4248984