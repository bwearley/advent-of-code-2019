use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::collections::VecDeque;
use std::collections::HashMap;

use intcode::intcode::IntcodeComputer;
extern crate intcode;

extern crate point2d;
use point2d::point2d::Point2D;

pub enum BotStatus {
    Wall = 0,
    Ok = 1,
    Complete = 2,
    Bad = 3,
}
impl BotStatus {
    pub fn from_int(value: i64) -> BotStatus {
        match value {
            0 => BotStatus::Wall,
            1 => BotStatus::Ok,
            2 => BotStatus::Complete,
            _ => BotStatus::Bad,
        }
    }
}

pub enum Direction {
    North = 1,
    South = 2,
    West = 3,
    East = 4
}
impl Direction {
    pub fn from_int(value: i64) -> Direction {
        match value {
            1 => Direction::North,
            2 => Direction::South,
            3 => Direction::West,
            4 => Direction::East,
            _ => panic!("Unknown value: {}", value),
        }
    }
}

#[derive(Clone)]
struct State {
    pos: Point2D,
    vm: IntcodeComputer,
    n: i64,
}
impl State {
    pub fn new(n: i64, vm: IntcodeComputer, pos: Point2D) -> State {
        State {
            n: n,
            vm: vm,
            pos: pos,
        }
    }
}

fn day15(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let input = input.concat();
    let intcode = input
        .split(',')
        .map(|x| x.parse::<i64>().unwrap())
        .collect::<Vec<i64>>();

    // Initialize & Configure Base VM
    let base_vm = IntcodeComputer::new(intcode);

    // Part 1
    let mut part1 = 0;
    let mut map: HashMap<Point2D,i64> = HashMap::new();
    let mut part2vm: IntcodeComputer = IntcodeComputer::new(vec![0]); // Save state of Intcode VM from end of part 1
    let mut q: VecDeque<State> = VecDeque::new();
    
    let initial_state = State::new(0, base_vm.clone(), Point2D::new(0,0));
    q.push_back(initial_state);
    while q.len() != 0 {
        let now = q.pop_front().unwrap();

        for dir in 1..=4 {
            let mut next = now.clone();

            match Direction::from_int(dir) {
                Direction::North => next.pos.y += 1,
                Direction::South => next.pos.y -= 1,
                Direction::East  => next.pos.x -= 1,
                Direction::West  => next.pos.x += 1,
            }

            // Process VM
            next.vm.push_input(dir);
            next.vm.run();
            match BotStatus::from_int(next.vm.pop_output()) {
                BotStatus::Ok => {
                    if !map.contains_key(&next.pos) {
                        map.insert(next.pos,next.n+1);
                        q.push_back(State::new(next.n+1,next.vm.clone(), next.pos));
                    }
                },
                BotStatus::Complete => {
                    part1 = next.n+1;
                    part2vm = next.vm.clone();
                    break;
                },
                BotStatus::Wall => {},
                BotStatus::Bad => panic!("Bad status") 
            }
        }
    }

    // Part 2
    map.clear();
    q.clear();
    let initial_state = State::new(0, part2vm.clone(), Point2D::new(0,0));
    q.push_back(initial_state);
    while q.len() != 0 {
        let now = q.pop_front().unwrap();

        for dir in 1..=4 {
            let mut next = now.clone();

            match Direction::from_int(dir) {
                Direction::North => next.pos.y += 1,
                Direction::South => next.pos.y -= 1,
                Direction::East  => next.pos.x -= 1,
                Direction::West  => next.pos.x += 1,
            }

            // Process VM
            next.vm.push_input(dir);
            next.vm.run();
            match BotStatus::from_int(next.vm.pop_output()) {
                BotStatus::Ok => {
                    if !map.contains_key(&next.pos) {
                        map.insert(next.pos,next.n+1);
                        q.push_back(State::new(next.n+1,next.vm.clone(), next.pos));
                    }
                },
                BotStatus::Complete => {
                    
                },
                BotStatus::Wall => {},
                BotStatus::Bad => panic!("Bad status") 
            }
        }
    }

    let part2 = map.values().max().unwrap();

    println!("Part 1: {}", part1); // 232
    println!("Part 2: {}", part2); // 320
    
    Ok(())
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day15(&filename).unwrap();
}