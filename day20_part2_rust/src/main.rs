use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::collections::VecDeque;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fmt;

#[macro_use] extern crate cached;

type MazeMap = HashMap<(i64,i64),MapPoint>;

#[macro_use] extern crate lazy_static;
lazy_static! {
    static ref MAZE_MAP: MazeMap = {
        let args: Vec<String> = env::args().collect();
        let filename = &args[1];
        let file = File::open(filename).expect("Input file not found.");
        let reader = BufReader::new(file);
        // Process input file char by char
        let mut maze: MazeMap = MazeMap::new();
        for (y,line) in reader.lines().enumerate() {
            for (x,c) in line.unwrap().chars().enumerate() {
                let (x,y) = (x as i64, y as i64);
                maze.insert((x,y),MapPoint::new(x,y,c));
            }
        }
        // Identify map limits
        let xmin = maze.keys().map(|k| k.0).min().expect("Failed to determine minimum x-dimension.");
        let xmax = maze.keys().map(|k| k.0).max().expect("Failed to determine maximum x-dimension.");
        let ymin = maze.keys().map(|k| k.1).min().expect("Failed to determine minimum y-dimension.");
        let ymax = maze.keys().map(|k| k.1).max().expect("Failed to determine maximum y-dimension.");

        // Identify portal locations
        for y in ymin..ymax {
            for x in xmin..xmax {
                let this  = maze.get(&(x+0,y)).unwrap().clone();
                let nextH = maze.get(&(x+1,y)).unwrap().clone();
                let nextV = maze.get(&(x,y+1)).unwrap().clone();
                // Horizontal
                if this.kind == MapType::PortalID && nextH.kind == MapType::PortalID {
                    let mut name = String::new();
                    name.push(this.name); name.push(nextH.name);
                    let left = match maze.get(&(x-1,y)) {
                        Some(d) => { if d.kind == MapType::Open { true } else { false }},
                        _ => false,
                    };
                    let right = match maze.get(&(x+2,y)) {
                        Some(d) => { if d.kind == MapType::Open { true } else { false }},
                        _ => false,
                    };
                    if left  { maze.insert((x-1,y), MapPoint::new_portal(x-1, y, '*', &name)); }
                    if right { maze.insert((x+2,y), MapPoint::new_portal(x+2, y, '*', &name)); }
                }
                // Vertical
                if this.kind == MapType::PortalID && nextV.kind == MapType::PortalID {
                    let mut name = String::new();
                    name.push(this.name); name.push(nextV.name);
                    let up = match maze.get(&(x,y-1)) {
                        Some(d) => { if d.kind == MapType::Open { true } else { false }},
                        _ => false,
                    };
                    let down = match maze.get(&(x,y+2)) {
                        Some(d) => { if d.kind == MapType::Open { true } else { false }},
                        _ => false,
                    };
                    if up   { maze.insert((x,y-1), MapPoint::new_portal(x, y-1, '*', &name)); }
                    if down { maze.insert((x,y+2), MapPoint::new_portal(x, y+2, '*', &name)); }
                }
            }
        }
        
        // Identify portals
        let portals: Vec<((i64,i64),MapPoint)> = maze
            .iter()
            .filter(|(_,v)| v.kind == MapType::Portal)
            .map(|(k,v)| (*k,MapPoint::clone_from(v)))
            .collect();
        // Assign portal targets
        for (xy,p) in portals.iter().filter(|(_,v)| v.portal_name != "AA" && v.portal_name != "ZZ") {
            let mut p_copy = MapPoint::clone_from(p);
            let other = &portals
                .iter()
                .filter(|(k,v)| v.portal_name == p.portal_name && !(k.0 == xy.0 && k.1 == xy.1))
                .map(|(k,_)| k)
                .next()
                .unwrap();
            p_copy.dest_x = other.0;
            p_copy.dest_y = other.1;
            maze.insert((p.x,p.y),p_copy);
        }
        maze
    };
}

fn is_outer_portal(pt: (i64,i64)) -> bool {
    const OFFSET: i64 = 2; // Offset between map edges and any outer edge portal
    let limits = map_limits();
    pt.0 == OFFSET || pt.1 == OFFSET || pt.0 == (limits.1-OFFSET) || pt.1 == (limits.3-OFFSET)
}
fn is_inner_portal(pt: (i64,i64)) -> bool { !is_outer_portal(pt) }

fn map_limits() -> (i64,i64,i64,i64) {
   (MAZE_MAP.keys().map(|k| k.0).min().expect("Failed to determine minimum x-dimension."),
    MAZE_MAP.keys().map(|k| k.0).max().expect("Failed to determine maximum x-dimension."),
    MAZE_MAP.keys().map(|k| k.1).min().expect("Failed to determine minimum y-dimension."),
    MAZE_MAP.keys().map(|k| k.1).max().expect("Failed to determine maximum y-dimension."))
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let debug = if args.len() >=3 { if &args[2] == "--debug" { true } else { false } } else { false };
    day20(debug).unwrap();
}

#[derive(Debug, Copy, Clone, PartialEq)]
enum MapType {
    Wall,
    Open,
    PortalID,
    Space,
    Portal,
}
impl MapType {
    pub fn from_char(c: char) -> MapType {
        match c {
            '#'       => MapType::Wall,
            '.'       => MapType::Open,
            ' '       => MapType::Space,
            '*'       => MapType::Portal,
            'A'..='Z' => MapType::PortalID,
            other => panic!("Unknown map character: {}", other),
        }
    }
}

#[derive(Debug, Clone)]
struct MapPoint {
    x: i64,
    y: i64,
    name: char,
    kind: MapType,
    dest_x: i64,
    dest_y: i64,
    portal_name: String,
}
impl MapPoint {
    pub fn new(x: i64, y: i64, c: char) -> MapPoint {
        MapPoint {
            x: x,
            y: y,
            name: c,
            kind: MapType::from_char(c),
            dest_x: 0,
            dest_y: 0,
            portal_name: String::new(),
        }
    }
    pub fn new_portal(x: i64, y: i64, c: char, portal_name: &String) -> MapPoint {
        MapPoint {
            x: x,
            y: y,
            name: c,
            kind: MapType::from_char(c),
            dest_x: 0,
            dest_y: 0,
            portal_name: portal_name.clone(),
        }
    }
    pub fn clone_from(other: &MapPoint) -> MapPoint {
        MapPoint {
            x: other.x,
            y: other.y,
            name: other.name,
            kind: other.kind,
            dest_x: other.dest_x,
            dest_y: other.dest_y,
            portal_name: other.portal_name.clone(),
        }
    }
}
impl fmt::Display for MapPoint {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({},{})->{}", self.x, self.y, self.name)
    }
}

fn valid_neighbors(x: i64, y: i64, z: i64) -> Vec<(i64,i64,i64)> {
    let mut neighbors: Vec<(i64,i64,i64)> = Vec::new();
    let newpt = (x-1,y  ,z); if is_open(&newpt) { neighbors.push(newpt); }
    let newpt = (x+1,y  ,z); if is_open(&newpt) { neighbors.push(newpt); }
    let newpt = (x  ,y-1,z); if is_open(&newpt) { neighbors.push(newpt); }
    let newpt = (x  ,y+1,z); if is_open(&newpt) { neighbors.push(newpt); }
    let cell = MAZE_MAP.get(&(x,y)).unwrap();
    match cell.kind {
        MapType::Portal => {
            if portal_usable(x,y,z) {
                let newpt = if is_inner_portal((x,y)) {
                    (cell.dest_x, cell.dest_y, z+1)
                } else {
                    (cell.dest_x, cell.dest_y, z-1)
                };
                neighbors.push(newpt);
            }            
        },
        _ => {},
    }
    neighbors
}

fn is_open(pt: &(i64,i64,i64)) -> bool {
    let cell = &MAZE_MAP.get(&(pt.0,pt.1)).unwrap();
    cell.kind == MapType::Open || cell.kind == MapType::Portal
}

fn portal_usable(x: i64, y: i64, z: i64) -> bool {
    let cell = MAZE_MAP.get(&(x,y)).unwrap();
    if cell.dest_x == 0 && cell.dest_y == 0 { return false; } // AA and ZZ have dest set to (0,0)
    match cell.kind {
        MapType::Portal => {
            // First floor
            if z == 0 {
                if is_inner_portal((x,y)) { true }
                else { false }
            // Other floors
            } else {
                match cell.portal_name.as_ref() {
                    "AA" | "ZZ" => false,
                    _ => true,
                }
            }
        },
        _ => false,
    }
}

cached! {
    PD;
    fn point_distance(x0: i64, y0: i64, z0: i64, x1: i64, y1: i64, z1: i64) -> usize = {
        let mut seen: HashSet<(i64,i64,i64)> = HashSet::new();

        // Insert first point
        let mut q: VecDeque<State> = VecDeque::new();
        q.push_back(State::new(x0, y0, z0, 0));

        // Queue
        let mut steps = std::usize::MAX;
        while q.len() > 0 {
            
            let node = q.pop_front().unwrap();
            if node.x == x1 && node.y == y1 && node.z == z1 {
                if node.steps < steps { steps = node.steps; }
                break;
            }
           
            for neighbor in valid_neighbors(node.x, node.y, node.z) {
                let neighbor_cell = &MAZE_MAP.get(&(neighbor.0, neighbor.1)).unwrap();

                if !seen.contains(&(neighbor.0,neighbor.1,neighbor.2)) {
                    seen.insert((neighbor.0,neighbor.1,neighbor.2));
                    let new_node = State::new(neighbor.0, neighbor.1, neighbor.2, &node.steps+1);
                    q.push_back(new_node);
                }
            }
        }
        steps
    }
}

#[derive(Debug, Clone)]
struct State {
    x: i64,
    y: i64,
    z: i64,
    steps: usize,
}
impl State {
    pub fn new(x: i64, y: i64, z: i64, steps: usize) -> State {
        State {
            x: x,
            y: y,
            z: z,
            steps: steps,
        }
    }
}

fn day20(debug: bool) -> io::Result<()> {

    let aa = &MAZE_MAP.iter().filter(|(_,v)| v.portal_name == "AA").map(|(k,_)| k).next().unwrap();
    let zz = &MAZE_MAP.iter().filter(|(_,v)| v.portal_name == "ZZ").map(|(k,_)| k).next().unwrap();
   
    if debug {
        print_map();

        println!("AA: ({},{})", aa.0, aa.1);
        println!("ZZ: ({},{})", zz.0, zz.1);

        println!("Portals:");
        let portals = &MAZE_MAP.iter().filter(|(_,v)| v.kind == MapType::Portal).collect::<Vec<_>>();
        for (xy,p) in portals.iter() {
            println!("{}: ({},{})->({},{}) [{}]", p.portal_name, xy.0, xy.1, p.dest_x, p.dest_y, is_inner_portal((xy.0,xy.1)));
        }
    }

    // Part 2
    let part2 = point_distance(aa.0, aa.1, 0, zz.0, zz.1, 0);
    println!("Part 2: {}", part2); // 5744

    Ok(())
}

fn print_map() {
    let (xmin,xmax,ymin,ymax) = map_limits();
    println!("Maze Map:");
    // Print x-values across top
    print!("   "); for x in xmin..=xmax { print!("{}", (x/100) % 10); } println!();
    print!("   "); for x in xmin..=xmax { print!("{}", (x/10)  % 10); } println!();
    print!("   "); for x in xmin..=xmax { print!("{}", (x/1)   % 10); } println!();
    // Print map
    for y in ymin..=ymax {
        let y001 = (y/1)   % 10;
        let y010 = (y/10)  % 10;
        let y100 = (y/100) % 10;
        print!("{}{}{}", y100, y010, y001); // Print y-values down the sides
        for x in xmin..=xmax {
            let cell = &MAZE_MAP.get(&(x,y)).unwrap();
            print!("{}", cell.name);
        }
        println!();
    }
}