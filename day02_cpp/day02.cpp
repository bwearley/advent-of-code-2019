#include <iostream>
#include <fstream>
#include <vector>

#include "../shared_cpp/stringtools.cpp"
#include "../intcode_cpp/intcode.cpp"

void Day02(std::string file)
{
    std::ifstream in(file);
    std::vector<int64_t> input;
    // Load file data
    while (!in.eof()) {
        std::string line;
        std::getline(in, line);
        auto numStr = split(line,',');
        for (auto & num : numStr) {
            input.push_back(std::stol(num));
        }
    }

    // Initialize & Configure VM
    auto vm = IntcodeComputer(input);

    // Part 1
    vm.set_addr(1,12);
    vm.set_addr(2,2);
    vm.run();
    int64_t part1 = vm.intcode[0];
    printf("Part 1: %lld\n", part1); // 7210630

    // Part 2
    int64_t part2 = 0;
    for (auto noun = 0; noun <= 99; noun++) {
        for (auto verb = 0; verb <= 99; verb++) {
            vm.reset();
            vm.set_addr(1,noun);
            vm.set_addr(2,verb);
            vm.run();
            if (vm.intcode[0] == 19690720) {
                part2 = 100 * noun + verb;
                break ;
            }
        }
        if (part2 != 0) break;
    }
    printf("Part 2: %lld\n", part2); // 3892

}

int main(int argc, char** argv)
{
  // Check for input file
  if (argc != 2) {
    printf("Must specify file at command line.\n");
    return -1;
  }

  Day02(argv[1]);
  return 0;
}

/*int64_t process(std::vector<int64_t> intcode, int64_t noun, int64_t verb) {
    int64_t ptr = 0;
    intcode[1] = noun;
    intcode[2] = verb;
    int64_t p1, p2, d;
    for (;;) {
        switch(intcode[ptr]) {
            case 1:
                p1 = intcode[ptr+1];
                p2 = intcode[ptr+2];
                d  = intcode[ptr+3];
                intcode[d] = intcode[p1] + intcode[p2];
                ptr += 4;
                break;
            case 2:
                p1 = intcode[ptr+1];
                p2 = intcode[ptr+2];
                d  = intcode[ptr+3];
                intcode[d] = intcode[p1] * intcode[p2];
                ptr += 4;
                break;
            case 99:
                return intcode[0];
                break;
            default:
                printf("ERROR: Unknown instruction: %lld\n", intcode[ptr]);
                exit(EXIT_FAILURE);
                break;
        }
    }
}*/