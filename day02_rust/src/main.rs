use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

use intcode::intcode::IntcodeComputer;
extern crate intcode;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day02(&filename).unwrap();
}

fn day02(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let input = input.concat();
    let intcode = input
        .split(',')
        .map(|x| x.parse::<i64>().unwrap())
        .collect::<Vec<i64>>();

    let mut vm = IntcodeComputer::new(intcode);

    // Part 1
    vm.set_addr(1,12);
    vm.set_addr(2,2);
    vm.run();
    let part1 = vm.get_addr(0);
    println!("Part 1: {}", part1); // 7210630

    // Part 2
    let mut part2 = 0;
    'noun_lp: for noun in 0..=99 {
        for verb in 0..=99 {
            vm.reset();
            vm.set_addr(1,noun);
            vm.set_addr(2,verb);
            vm.run();
            if vm.intcode[0] == 1969_07_20 {
                part2 = 100 * noun + verb;
                break 'noun_lp;
            }
        }
    }
    println!("Part 2: {}", part2); // 3892

    Ok(())
}