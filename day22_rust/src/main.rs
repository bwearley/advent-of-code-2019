use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use mod_exp::mod_exp;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day22(&filename).unwrap();
}

#[derive(Copy, Clone)]
enum Operation {
    DealNewStack,
    Cut(i128),
    DealWithIncrement(i128),
}

fn part1(ops: &[Operation]) -> i128 {
    const NUM_CARDS: i128 = 10007;
    let mut pos: i128 = 2019;
    let _ = ops
        .iter()
        .map(|o| {
            match o {
                Operation::DealWithIncrement(n) => {pos = pos * n % NUM_CARDS},
                Operation::DealNewStack => { pos = NUM_CARDS - 1 - pos},
                Operation::Cut(n) => {pos = (pos - n) % NUM_CARDS},
            }
        })
        .collect::<Vec<_>>();
    pos
}

fn part2(ops: &[Operation]) -> i128 {
    const NUM_CARDS: i128 = 119_315_717_514_047;
    const NUM_SHUFFLES: i128 = 101_741_582_076_661;
    const CARD_POS: i128 = 2020;

    // https://www.reddit.com/r/adventofcode/comments/ee0rqi/2019_day_22_solutions/fbnkaju/
    // ax + b
    let (a,b) = ops
        .iter()
        .rev()
        .fold((1,0), |(a,b), &op| {
            let (a2,b2) = match op {
                Operation::DealNewStack => (-a, -b-1),
                Operation::Cut(n) => (a, b+n),
                Operation::DealWithIncrement(n) => {
                    let n = mod_exp(n, NUM_CARDS-2, NUM_CARDS);
                    (a * n, b * n)
                },
            };
            (a2 % NUM_CARDS, b2 % NUM_CARDS)
        });

    // x * a^n + b * (a^n-1) / (a-1)
    let t1 = CARD_POS * mod_exp(a,NUM_SHUFFLES,NUM_CARDS) % NUM_CARDS;
    let t00 = ((mod_exp(a,NUM_SHUFFLES,NUM_CARDS) - 1) * mod_exp(a-1, NUM_CARDS-2, NUM_CARDS)) % NUM_CARDS;
    let t01 = t00 % NUM_CARDS;
    let t2 = b * t01 % NUM_CARDS;
    
    (t1 + t2) % NUM_CARDS
}

fn day22(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    let ops = input
        .iter()
        .map(|x| {
                let parts: Vec<_> = x.split_whitespace().collect();
                match (parts[0],parts[1]) {
                    //deal with increment 64
                    //deal into new stack
                    //cut 1004
                    ("deal","with") => Operation::DealWithIncrement(parts[3].parse::<i128>().unwrap()),
                    ("deal","into") => Operation::DealNewStack,
                    ("cut", _)      => Operation::Cut(parts[1].parse::<i128>().unwrap()),
                    (_,_)           => panic!("Unknown command: {}", x),
                }
            }
            )
        .collect::<Vec<Operation>>();

    let part1 = part1(&ops);
    println!("Part 1: {}", part1); // 8502

    let part2 = part2(&ops);
    println!("Part 2: {}", part2); // 41685581334351

    Ok(())
}