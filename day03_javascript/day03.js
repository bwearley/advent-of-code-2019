'use strict';

const fs = require('fs');
const chalk = require('chalk');
const HashMap = require('hashmap');

var args = process.argv.slice(2);

function manhattan_distance(x0,y0,x1,y1) {
    return Math.abs(x1-x0) + Math.abs(y1-y0);
}

const DIR = {
    'U': [0,-1],
    'D': [0,+1],
    'L': [-1,0],
    'R': [+1,0],
};

const sumArrays = (arr1,arr2) => [arr1,arr2].reduce((r, a) => a.map((b, i) => (r[i] || 0) + b), [])

function day03(filename) {

    const input = fs.readFileSync(filename)
        .toString()
        .split("\n")
        .filter(s => s.length > 0);

    var wires = [];
    var wire_map = new HashMap; // k,v = point, cross #
    input.map(line => {
        var parts = line.split(",");
        var wire = new HashMap; // k,v = point, distance
        var pos = [0,0];
        var d = 0;
        // Build map of this wire
        for (const segment of parts) {
            var dir = DIR[segment[0]];
            var mag = Number(segment.slice(1));
            for (var i = 0; i < mag; i++) {
                pos = sumArrays(pos,dir);
                d += 1;
                if (!wire.has(pos)) { wire.set(pos,d); }
            }
        }
        wires.push(wire);
        // Merge this wire's intersection into global wire map
        for (const pt of wire.keys()) {
            var ct = wire_map.get(pt);
            if (ct != null) {
                wire_map.set(pt,ct+1);
            } else {
                wire_map.set(pt,1);
            }
        }
    });

    const overlaps = wire_map.entries().filter(e => e[1] == 2).map(e => e[0]);

    // Part 1
    const part1 = Math.min(...overlaps.map(pt => manhattan_distance(0,0,pt[0],pt[1]) )); // 1211

    // Part 2
    var part2 = Number.MAX_SAFE_INTEGER;
    for (const overlap of overlaps) {      
        var dist = wires.map(w => w.get(overlap)).reduce((a,b) => a+b,0);
        part2 = Math.min(part2, dist);
    }

    return [part1, part2];
}

const [part1,part2] = day03(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 1211
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 101386
