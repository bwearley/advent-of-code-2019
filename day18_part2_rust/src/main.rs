use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::collections::VecDeque;
use std::collections::HashMap;
use std::fmt;

#[macro_use] extern crate cached;
//use cached::SizedCache;

type CaveMap = HashMap<(i64,i64),MapPoint>;

const NUM_KEYS: usize = 26;

#[macro_use] extern crate lazy_static;
lazy_static! {
    static ref CAVE_MAP: CaveMap = {
        let args: Vec<String> = env::args().collect();
        let filename = &args[1];
        let file = File::open(filename).expect("Input file not found.");
        let reader = BufReader::new(file);
        let mut cmap: CaveMap = CaveMap::new();
        for (y,line) in reader.lines().enumerate() {
            for (x,c) in line.unwrap().chars().enumerate() {
                let (x,y) = (x as i64, y as i64);
                cmap.insert((x,y),MapPoint::new(x,y,c));
            }
        }
        cmap
    };
}

lazy_static! {
    static ref TARGET_MAP: HashMap<char,(i64,i64)> = {
        let targets: Vec<char> = (97..(97+NUM_KEYS)).map(|i| i as u8 as char).collect();
        let mut target_map: HashMap<char,(i64,i64)> = HashMap::new();
        for t in &targets {
            let tmp = &CAVE_MAP.iter().find(|(_,v)| v.name == *t).map(|(_,v)| v).unwrap();
            let (x,y) = (tmp.x,tmp.y);
            target_map.insert(*t,(x,y));
        }
        target_map
    };
}

fn ch_ix(ch: char) -> usize {
    match ch {
        'a'..='z' => ((ch as u8) - 96 - 1) as usize,
        'A'..='Z' => ((ch as u8) - 64 - 1) as usize,
        other => panic!("Unknown character: {}", other),
    }
}

fn key_from_num(ch_ix: usize) -> char {
    ((ch_ix + 96 + 1) as u8) as char
}

fn map_limits() -> (i64,i64,i64,i64) {
   (CAVE_MAP.iter().map(|(k,_)| k.0).min().expect("Failed to determine minimum x-dimension."),
    CAVE_MAP.iter().map(|(k,_)| k.0).max().expect("Failed to determine maximum x-dimension."),
    CAVE_MAP.iter().map(|(k,_)| k.1).min().expect("Failed to determine minimum y-dimension."),
    CAVE_MAP.iter().map(|(k,_)| k.1).max().expect("Failed to determine maximum y-dimension."))
}

fn main() {
    let args: Vec<String> = env::args().collect();
    //let filename = &args[1];
    let debug = if args.len() >=3 { if &args[2] == "--debug" { true } else { false } } else { false };
    day18(debug).unwrap();
}

#[derive(Debug, Copy, Clone, PartialEq)]
enum MapType {
    Wall,
    Open,
    Key,
    Door,
    Origin,
}
impl MapType {
    pub fn from_char(c: char) -> MapType {
        match c {
            '#'       => MapType::Wall,
            '.'       => MapType::Open,
            'a'..='z' => MapType::Key,
            'A'..='Z' => MapType::Door,
            '@'       => MapType::Origin,
            other => panic!("Unknown map character: {}", other),
        }
    }
}

#[derive(Debug, Copy, Clone)]
struct MapPoint {
    x: i64,
    y: i64,
    name: char,
    kind: MapType,
}
impl MapPoint {
    pub fn new(x: i64, y: i64, c: char) -> MapPoint {
        MapPoint {
            x: x,
            y: y,
            name: c,
            kind: MapType::from_char(c),
        }
    }
}
impl fmt::Display for MapPoint {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({},{})->{}", self.x, self.y, self.name)
    }
}

fn valid_neighbors(x: i64, y: i64, available_keys: [bool; NUM_KEYS]) -> Vec<(i64,i64)> {
    let mut neighbors: Vec<(i64,i64)> = Vec::new();
    let newpt = (x-1,y  ); if is_open(&newpt, available_keys) { neighbors.push(newpt); }
    let newpt = (x+1,y  ); if is_open(&newpt, available_keys) { neighbors.push(newpt); }
    let newpt = (x  ,y-1); if is_open(&newpt, available_keys) { neighbors.push(newpt); }
    let newpt = (x  ,y+1); if is_open(&newpt, available_keys) { neighbors.push(newpt); }
    neighbors
}

fn is_open(pt: &(i64,i64), available_keys: [bool; NUM_KEYS]) -> bool {
    let cell = &CAVE_MAP.get(&pt).unwrap();
    cell.kind == MapType::Open   || 
    cell.kind == MapType::Key    ||
    cell.kind == MapType::Origin ||
   (cell.kind == MapType::Door && available_keys[ch_ix(cell.name)])
}

cached! {
    PD;
    fn point_distance(x0: i64, y0: i64, x1: i64, y1: i64, available_keys: [bool; NUM_KEYS]) -> usize = {
       
        // Distance map
        let (_,xmax,_,ymax) = map_limits();
        let mut distance: Vec<Vec<usize>> = vec![vec![std::usize::MAX; ymax as usize]; xmax as usize];

        // Insert first point
        let mut q: VecDeque<State> = VecDeque::new();
        q.push_back(State::new(x0, y0, 0, available_keys));

        // Queue
        let mut steps = std::usize::MAX;
        while q.len() > 0 {
            
            let node = q.pop_front().unwrap();
            if node.x == x1 && node.y == y1 {
                if node.steps < steps { steps = node.steps; }
                break;
            }

            for neighbor in valid_neighbors(node.x, node.y, node.keys) {
                let neighbor_cell = &CAVE_MAP.get(&(neighbor.0, neighbor.1)).unwrap();
                let new_dist = &node.steps + 1;

                // Don't want to run into other non-target keys along the way
                if neighbor_cell.kind == MapType::Key          &&
                    !available_keys[ch_ix(neighbor_cell.name)] &&
                    !(neighbor_cell.x == x1 && neighbor_cell.y == y1) {
                    continue;
                }

                // Collect key if this is a key
                let mut keys = available_keys;
                if neighbor_cell.kind == MapType::Key { keys[ch_ix(neighbor_cell.name)] = true; }

                if new_dist < distance[neighbor.0 as usize][neighbor.1 as usize] {
                    distance[neighbor.0 as usize][neighbor.1 as usize] = new_dist;
                    let new_node = State::new(neighbor.0, neighbor.1, new_dist, keys);
                    q.push_back(new_node);
                }
            }
        }
        steps
    }
}

cached!{ SCORE;
    fn score(x: i64, y: i64, steps: usize, keys: [bool; NUM_KEYS]) -> usize = {
        let reachable: Vec<char> = keys
            .iter()
            .enumerate()
            .filter(|(_,k)| !**k)
            .filter(|(kix,_)| {
                let tgt_ch = key_from_num(*kix);
                let tgt_xy = TARGET_MAP.get(&tgt_ch).unwrap();
                point_distance(x, y, tgt_xy.0, tgt_xy.1, keys) != std::usize::MAX
            })
            .map(|(kix,_)| key_from_num(kix))
            .collect();

        reachable
            .iter()
            .map(|t| {
                let tgt_xy = TARGET_MAP.get(&t).unwrap();
                let mut new_keys = keys;
                new_keys[ch_ix(*t)] = true;
                let new_dist = point_distance(x, y, tgt_xy.0, tgt_xy.1, keys);
                steps + score(tgt_xy.0, tgt_xy.1, new_dist, new_keys)
            })
            .min()
            .unwrap_or(steps)
    }
}

#[derive(Debug, Clone)]
struct State {
    x: i64,
    y: i64,
    steps: usize,
    keys: [bool; NUM_KEYS],
}
impl State {
    pub fn new(x: i64, y: i64, steps: usize, keys: [bool; NUM_KEYS]) -> State {
        State {
            x: x,
            y: y,
            steps: steps,
            keys: keys,
        }
    }
}

fn day18(debug: bool) -> io::Result<()> {

    let origins: Vec<_> = CAVE_MAP.iter().filter(|(_,v)| v.name == '@').map(|(k,_)| k).collect();
    let mut origins: Vec<(i64, i64)> = origins.iter().map(|org| (org.0, org.1)).collect();
    origins.sort();
    // Quadrants
    // 0 2
    // 1 3

    let doors: Vec<_> = CAVE_MAP.iter().filter(|(_,v)| v.kind == MapType::Door).collect();
    let keys: Vec<_> = CAVE_MAP.iter().filter(|(_,v)| v.kind == MapType::Key).collect();
    let num_keys = keys.len();
    let num_doors = doors.len();
    if num_keys != NUM_KEYS {
        panic!("Error: Found keys ({}) =/= NUM_KEYS ({})", num_keys, NUM_KEYS);
    }

    // Draw Map
    if debug {
        print_map();

        for (ix,origin) in origins.iter().enumerate() {
            println!("Origin {} ({},{})", ix, origin.0, origin.1);
        }
        
        println!("Keys: ({})", num_keys);
        for (k,v) in &keys {
            println!("({},{}) -> {}", k.0, k.1, v);
        }

        println!("Doors: ({})", num_doors);
        for (k,v) in &doors {
            println!("({},{}) -> {}", k.0, k.1, v);
        }
    }

    // Determine quadrant dimensions
    let (xmin,xmax,ymin,ymax) = map_limits();  // Quadrants
    let xmid = (xmax-xmin) / 2;                // 0 2
    let ymid = (ymax-ymin) / 2;                // 1 3

    // Determine which keys are found in which quadrants
    let keys_q0: Vec<char> = keys.iter().filter(|(k,_)| k.0 < xmid && k.1 < ymid).map(|(_,v)| v.name).collect();
    let keys_q1: Vec<char> = keys.iter().filter(|(k,_)| k.0 < xmid && k.1 > ymid).map(|(_,v)| v.name).collect();
    let keys_q2: Vec<char> = keys.iter().filter(|(k,_)| k.0 > xmid && k.1 < ymid).map(|(_,v)| v.name).collect();
    let keys_q3: Vec<char> = keys.iter().filter(|(k,_)| k.0 > xmid && k.1 > ymid).map(|(_,v)| v.name).collect();

    // Start each quadrant by providing keys which are from other quadrants
    let mut key_quadrants = vec![[true; NUM_KEYS];4];
    for k in keys_q0 { key_quadrants[0][ch_ix(k)] = false; }
    for k in keys_q1 { key_quadrants[1][ch_ix(k)] = false; }
    for k in keys_q2 { key_quadrants[2][ch_ix(k)] = false; }
    for k in keys_q3 { key_quadrants[3][ch_ix(k)] = false; }

    if debug {
        for (qi,q) in key_quadrants.iter().enumerate() {
            println!("Keys provided to Q{} ({},{})", qi, origins[qi].0, origins[qi].1);
            for (ki,k) in q.iter().enumerate() {
                if *k { print!(" {}",key_from_num(ki)); }
            }
            println!();
        }
    }

    // Part 2
    let part2: usize = origins
        .iter()
        .enumerate()
        .map(|(i,origin)| score(origin.0, origin.1, 0, key_quadrants[i]))
        .sum();
    println!("Part 2: {}", part2); // 1938

    Ok(())
}

fn print_map() {
    let (xmin,xmax,ymin,ymax) = map_limits();
    println!("Cave Map:");
    for y in ymin..=ymax {
        for x in xmin..=xmax {
            let cell = &CAVE_MAP.get(&(x,y)).unwrap();
            print!("{}", cell.name);
        }
        println!();
    }
}