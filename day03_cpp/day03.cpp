#include <iostream>
#include <fstream>
#include <array>
#include <vector>
#include <map>

#include "../shared_cpp/stringtools.cpp"

typedef int64_t i64;
typedef std::array<int64_t,2> i64pair;

i64 manhattan_distance(i64 x0,i64 y0,i64 x1,i64 y1) {
    return std::abs(x0-x1) + std::abs(y0-y1);
}

void Day03(std::string file) {
    std::ifstream in(file);
    std::vector<std::vector<std::string>> input;
    // Load file data
    while (!in.eof()) {
        std::string line;
        std::getline(in, line);
        input.push_back(split(line,','));
    }

    std::vector<std::map<i64pair,i64>> wires;
    std::map<i64pair,i64> wire_map; // k,v = point, cross #
    for (auto & line : input) {
        std::map<i64pair,i64> wire; // k,v = point, distance
        i64pair pos = {0, 0};
        i64pair dir = {0, 0};
        i64 d = 0;
        // Build map of this wire
        for (auto & seg : line) {
            auto dirStr = seg[0];
            i64 mag = std::stoi(seg.substr(1));
            if (dirStr == 'U') dir = { 0,-1};
            if (dirStr == 'D') dir = { 0,+1};
            if (dirStr == 'L') dir = {-1, 0};
            if (dirStr == 'R') dir = {+1, 0};
            if (dirStr != 'U' && dirStr != 'D' && dirStr != 'L' && dirStr != 'R') {
                std::cout << "ERROR: Unknown direction: " << dirStr << std::endl;
                exit(EXIT_FAILURE);
            }
            for (auto i = 0; i < mag; i++) {
                pos[0] += dir[0];
                pos[1] += dir[1];
                d += 1;
                wire.try_emplace(pos,d);
            }
        }
        wires.push_back(wire);
        // Merge this wire's intersection into global wire map
        for (const auto &pair : wire) {
            auto res = wire_map.try_emplace(pair.first,1);
            if (res.second == false) { res.first->second++; }
        }
    }

    // Part 1
    i64 part1 = INT64_MAX;
    for (const auto &pair : wire_map) {
        if (pair.second != 2) continue;
        part1 = std::min(part1, manhattan_distance(0,0,pair.first[0],pair.first[1]));
    }
    printf("Part 1: %lld\n", part1); // 1211

    // Part 2
    i64 part2 = INT64_MAX;
    for (const auto &pair : wire_map) {
        if (pair.second != 2) continue;
        auto dist = wires[0].find(pair.first)->second + wires[1].find(pair.first)->second;
        part2 = std::min(part2, dist);
    }
    printf("Part 2: %lld\n", part2); // 101386

}

int main(int argc, char** argv) {
    // Check for input file
    if (argc != 2) {
        printf("Must specify file at command line.\n");
        return -1;
    }

    Day03(argv[1]);
    return 0;
}