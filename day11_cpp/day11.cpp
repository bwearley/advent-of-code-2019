#include <iostream>
#include <fstream>
#include <vector>
#include <tuple>
#include <complex>
#include <map>

#include "../shared_cpp/stringtools.cpp"
#include "../intcode_cpp/intcode.cpp"

typedef std::int64_t i64;
typedef std::tuple<i64,i64> pt2d;

enum Color {
    Black = 0,
    White = 1,
};

std::tuple<i64,i64> tupleFromComplex(std::complex<i64> cmplx) {
    return std::tuple<i64,i64>(std::real(cmplx),std::imag(cmplx));
}

void Day11Part1(std::string file) {

    std::ifstream in(file);
    std::vector<i64> input;
    // Load file data
    while (!in.eof()) {
        std::string line;
        std::getline(in, line);
        auto numStr = split(line,',');
        for (auto & num : numStr) {
            input.push_back(std::stol(num));
        }
    }

    // Define directions
    std::complex<i64> turn_left(0, 1);
    std::complex<i64> turn_right(0, -1);

    // Define turns
    const i64 LEFT = 0;
    const i64 RIGHT = 1;

    // Initialize & Configure VM
    auto vm = IntcodeComputer(input);
    vm.break_on_out = true;
    //vm.debug = true;
    vm.push_input(Color::Black);

    // Initialize Map State
    std::map<pt2d,i64> img;
    std::complex<i64> pos(0,0);
    std::complex<i64> dir(0,-1);
    img.insert( std::pair<pt2d,i64>(tupleFromComplex(pos),Color::Black) );

    // Run robot
    while (vm.active) {
        vm.run();
        if (!vm.active) break;
        i64 color = vm.pop_output();
        vm.run();
        if (!vm.active) break;
        i64 turn = vm.pop_output();
        switch (turn) {
            case LEFT:
                dir *= turn_left;
                break;
            case RIGHT:
                dir *= turn_right;
                break;
            default:
                printf("Unknown direction: %lld\n", turn);
                exit(EXIT_FAILURE);
                return;
        }
        auto res = img.insert( std::pair<pt2d,i64>(tupleFromComplex(pos),color) );
        if (res.second == false) res.first->second = color;

        // Move
        pos += dir;

        // Start next
        auto it = img.find(tupleFromComplex(pos));
        if (it != img.end()) {
            vm.push_input(it->second);
        } else {
            vm.push_input(Color::Black);
        }
    }
    printf("Part 1: %lu\n", img.size()); // 2184

}

void Day11Part2(std::string file) {

    std::ifstream in(file);
    std::vector<i64> input;
    // Load file data
    while (!in.eof()) {
        std::string line;
        std::getline(in, line);
        auto numStr = split(line,',');
        for (auto & num : numStr) {
            input.push_back(std::stol(num));
        }
    }

    // Define directions
    std::complex<i64> turn_left(0, 1);
    std::complex<i64> turn_right(0, -1);

    // Define turns
    const i64 LEFT = 0;
    const i64 RIGHT = 1;

    // Initialize & Configure VM
    auto vm = IntcodeComputer(input);
    vm.break_on_out = true;
    //vm.debug = true;
    vm.push_input(Color::White);

    // Initialize Map State
    std::map<pt2d,i64> img;
    std::complex<i64> pos(0,0);
    std::complex<i64> dir(0,-1);
    img.insert( std::pair<pt2d,i64>(tupleFromComplex(pos),Color::White) );

    // Run robot
    while (vm.active) {
        vm.run();
        if (!vm.active) break;
        i64 color = vm.pop_output();
        vm.run();
        if (!vm.active) break;
        i64 turn = vm.pop_output();
        switch (turn) {
            case LEFT:
                dir *= turn_left;
                break;
            case RIGHT:
                dir *= turn_right;
                break;
            default:
                printf("Unknown direction: %lld\n", turn);
                exit(EXIT_FAILURE);
                return;
        }
        auto res = img.insert( std::pair<pt2d,i64>(tupleFromComplex(pos),color) );
        if (res.second == false) res.first->second = color;

        // Move
        pos += dir;

        // Start next
        auto it = img.find(tupleFromComplex(pos));
        if (it != img.end()) {
            vm.push_input(it->second);
        } else {
            vm.push_input(Color::Black);
        }
    }
  
    // Get image limits
    i64 xmin = INT64_MAX;
    i64 ymin = INT64_MAX;
    i64 xmax = -INT64_MAX;
    i64 ymax = -INT64_MAX;
    for (auto it = img.begin(); it != img.end(); ++it) {
        xmax = std::max(xmax, std::get<0>(it->first));
        ymax = std::max(ymax, std::get<1>(it->first));
        xmin = std::min(xmin, std::get<0>(it->first));
        ymin = std::min(ymin, std::get<1>(it->first));
    }

    // Draw
    printf("Part 2:\n"); // AHCHZEPK
    for (i64 y = ymin; y <= ymax; y++) {
        for (i64 x = xmax; x >= xmin; x--) {
            auto it = img.find(std::tuple<i64,i64>(x,y));
            i64 c;
            if (it != img.end()) {
                c = it->second;
            } else {
                c = Color::Black;
            }
            switch (c) {
                case Color::Black:
                    printf(" ");
                    break;
                case Color::White:
                    printf("#");
                    break;
                default:
                    printf(" ");
                    break;
            }
        }
        printf("\n");
    }
}

int main(int argc, char** argv) {
    // Check for input file
    if (argc != 2) {
        printf("Must specify file at command line.\n");
        return -1;
    }

    Day11Part1(argv[1]);
    Day11Part2(argv[1]);
    return 0;
}