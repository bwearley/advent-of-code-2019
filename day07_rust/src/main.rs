use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;

use permutator::{Permutation};
extern crate permutator;

use intcode::intcode::IntcodeComputer;
extern crate intcode;

fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day07(&filename).unwrap();
}

fn day07(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };
    let input = input.concat();
    let intcode = input
        .split(',')
        .map(|x| x.parse::<i64>().unwrap())
        .collect::<Vec<i64>>();

    // Make Amps
    let mut amps: Vec<IntcodeComputer> = (0..=4).map(|_| IntcodeComputer::new(intcode.clone())).collect();

    // Part 1
    let mut settings: Vec<i64> = (0..=4).collect();
    let mut part1 = 0;
    for permute in settings.permutation() {
        // Reset each computer and set initial inputs
        for (ix,s) in permute.iter().enumerate() {
            amps[ix].reset();
            amps[ix].push_input(*s);
        }

        // Run each computer
        let mut last = 0;
        for ix in 0..amps.len() {
            amps[ix].push_input(last);
            amps[ix].run();
            last = amps[ix].pop_output();
        }
        part1 = std::cmp::max(part1, last);
    }
    println!("Part 1: {}", part1); // 47064
 
    // Part 2
    let mut settings: Vec<i64> = (5..=9).collect();
    let mut part2 = 0;
    for permute in settings.permutation() {
        // Reset each computer and set initial inputs
        for (ix,s) in permute.iter().enumerate() {
            amps[ix].reset();
            amps[ix].push_input(*s);
        }

        // Run each computer
        let mut last = 0;
        'AMPS_LP: loop {
            for ix in 0..amps.len() {
                amps[ix].push_input(last);
                amps[ix].run();
                if !amps[ix].active { break 'AMPS_LP; }
                last = amps[ix].pop_output();
            }
        }
        part2 = std::cmp::max(part2, last);
    }
    println!("Part 2: {}", part2); // 4248984

    Ok(())
}