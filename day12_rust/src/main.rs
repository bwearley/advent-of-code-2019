use std::env;
use std::io::{self, prelude::*, BufReader};
use std::fs::File;
use std::fmt;

extern crate num;
use crate::num::Integer;

#[macro_use]
extern crate text_io;

extern crate point3d;
use point3d::point3d::Point3D;

#[derive(Debug, Copy, Clone)]
struct Moon {
    pub pos: Point3D,
    pub vel: Point3D,
    pub pos0: Point3D,
}
impl Moon {
    pub fn new(input: &str) -> Moon {
        let x: i64;
        let y: i64;
        let z: i64;
        scan!(input.bytes() => "<x={}, y={}, z={}>", x, y, z);
        Moon {
            pos: Point3D::new(x,y,z),
            vel: Point3D::new(0,0,0),
            pos0: Point3D::new(x,y,z),
        }
    }
    pub fn step(&mut self) {
        self.pos += self.vel;
    }
    pub fn pe(&self) -> i64 {
        self.pos.x.abs() + self.pos.y.abs() + self.pos.z.abs()
    }
    pub fn ke(&self) -> i64 {
        self.vel.x.abs() + self.vel.y.abs() + self.vel.z.abs()
    }
    pub fn energy(&self) -> i64 {
        self.pe() * self.ke()
    }
}
impl fmt::Display for Moon {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "pos=<x={}, y={}, z={}>, vel=<x={}, y={}, z={}>", 
        self.pos.x, self.pos.y, self.pos.z,
        self.vel.x, self.vel.y, self.vel.z)
    }
}

fn day12(input: &str) -> io::Result<()> {
    let file = File::open(input).expect("Input file not found.");
    let reader = BufReader::new(file);

    let input: Vec<String> = match reader.lines().collect() {
        Err(err) => panic!("Unknown error reading input: {}", err),
        Ok(result) => result,
    };

    let mut moons: Vec<Moon> = input.iter().map(|line| Moon::new(line)).collect();
    let num_moons = moons.len();

    // Part 1 Properties
    let part1_steps = 1000;
    let mut part1 = 0;

    // Part 2 Properties
    let mut periods = Point3D::zeros();

    let mut step = 0;
    loop {

        // Debug
        //println!("After {} steps:", step);
        //for m in &moons { println!("{}", m); }
        //println!("Total energy: {}", moons.iter().map(|m| m.energy()).sum::<i64>());

        // Assess part 1
        if step == part1_steps { part1 = moons.iter().map(|m| m.energy()).sum::<i64>(); }

        // Update moon velocities
        for ix in 0..num_moons {
            let mut dv = Point3D::zeros();
            for other in &moons { // doesn't exclude self, but comparison with self is zero anyway
                // Loop x, y, z
                dv.x += (other.pos.x-moons[ix].pos.x).signum();
                dv.y += (other.pos.y-moons[ix].pos.y).signum();
                dv.z += (other.pos.z-moons[ix].pos.z).signum();
            }
            moons[ix].vel += dv;
        }

        // Update moon positions
        for moon in &mut moons { moon.step(); }
        step += 1;

        // Check if each axis matches initial state
        // (x,y,z axis motions are completely independent)
        if periods.x == 0 && moons.iter().all(|m| m.vel.x == 0) && moons.iter().all(|m| m.pos.x == m.pos0.x) { periods.x = step; }
        if periods.y == 0 && moons.iter().all(|m| m.vel.y == 0) && moons.iter().all(|m| m.pos.y == m.pos0.y) { periods.y = step; }
        if periods.z == 0 && moons.iter().all(|m| m.vel.z == 0) && moons.iter().all(|m| m.pos.z == m.pos0.z) { periods.z = step; }
        if periods.x != 0 && periods.y != 0 && periods.z != 0 { break; }
        
    }

    let periods: Vec<i64> = vec![periods.x,periods.y,periods.z];
    let part2 = periods.iter().fold(1, |acc: i64, x| acc.lcm(x));

    println!("Part 1: {}", part1); // 8625
    println!("Part 2: {}", part2); // 332477126821644

    Ok(())
}


fn main() {
    let args: Vec<String> = env::args().collect();
    let filename = &args[1];
    day12(&filename).unwrap();
}