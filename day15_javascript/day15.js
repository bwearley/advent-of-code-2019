'use strict';
const fs = require('fs');
const chalk = require('chalk');
const dequeue = require('dequeue');
const hashmap = require('hashmap')
const clone = require('lodash.clone')
const clonedeep = require('lodash.clonedeep')

var {IntcodeComputer} = require('../intcode_js/intcode.js');

var args = process.argv.slice(2);

const STATUS_WALL = 0;
const STATUS_OK = 1;
const STATUS_COMPLETE = 2;

class State {
    constructor(n,vm,x,y) {
        this.pos = [x,y];
        this.vm = vm;
        this.n = n;
    }
}

function day15(filename) {

    // Load input
    const input = fs.readFileSync(filename)
        .toString()
        .split(",")
        .map(n => Number(n));
    
    // Initialize & Configure Base VM
    var vm = new IntcodeComputer(input);
    vm.break_on_out = true;
    vm.break_on_in = true;

    // Define directions
    const NORTH = 1;
    const SOUTH = 2;
    const WEST = 3;
    const EAST = 4;
    const dirs = [NORTH, SOUTH, EAST, WEST];

    // Part 1
    var part1 = Number.MAX_SAFE_INTEGER;
    var map = new hashmap();
    var part2vm; // Save state of Intcode VM from end of part 1

    var initialState = new State(0,clonedeep(vm),0,0);
    var q = new dequeue();
    map.set([0,0],0);
    q.push(initialState);
    while (q.length != 0) {
        var now = q.pop();
        for (const dir of dirs) {
            var next = clonedeep(now);
            
            // Process direction
            switch (dir) {
                case NORTH: next.pos[1] += 1; break;
                case SOUTH: next.pos[1] -= 1; break;
                case WEST:  next.pos[0] -= 1; break;
                case EAST:  next.pos[0] += 1; break;
                default: break;
            }
            
            // Process VM
            next.vm.push_input(dir);
            next.vm.run();
            var status = next.vm.pop_output();
            var [newx,newy] = next.pos;

            // Check status and continue
            switch (status) {
                case STATUS_OK:
                    if (!map.has([newx,newy]) || map.get([newx,newy]) > next.n+1) {
                        map.set([newx,newy],next.n+1)
                        q.push(new State(next.n+1,clonedeep(next.vm),newx,newy))
                    }
                    break;
                case STATUS_COMPLETE:
                    part1 = next.n+1;
                    part2vm = next.vm;
                    break;
                case STATUS_WALL:
                    continue;
                default:
                    console.log(`Error: Bad status: ${status}`);
                    break;
            }
        }
    }

    // Part 2
    var initialState = new State(0,clonedeep(part2vm),0,0);
    q.empty();
    map.clear();
    map.set([0,0],0);
    q.push(initialState)
    while (q.length != 0) {
        var now = q.pop();

        for (const dir of dirs) {
            var next = clonedeep(now);

            // Process direction
            switch (dir) {
                case NORTH: next.pos[1] += 1; break;
                case SOUTH: next.pos[1] -= 1; break;
                case WEST:  next.pos[0] -= 1; break;
                case EAST:  next.pos[0] += 1; break;
                default: break;
            }
            
            // Process VM
            next.vm.push_input(dir);
            next.vm.run();
            var status = next.vm.pop_output();
            var [newx,newy] = next.pos;

            // Check status and continue
            switch (status) {
                case STATUS_OK:
                    if (!map.has([newx,newy]) || map.get([newx,newy]) > next.n+1) {
                        map.set([newx,newy],next.n+1)
                        q.push(new State(next.n+1,clonedeep(next.vm),newx,newy))
                    }
                    break;
                case STATUS_COMPLETE: continue;
                case STATUS_WALL: continue; 
                default:
                    console.log(`Error: Bad status: ${status}`);
                    break;
            }
        }
    }
    const part2 = Number(Math.max(...map.entries().map(e => e[1])));

    return [part1,part2];
}

const [part1,part2] = day15(args[0]);
console.log(chalk.blue("Part 1: ") + chalk.yellow(part1)); // 232
console.log(chalk.blue("Part 2: ") + chalk.yellow(part2)); // 320