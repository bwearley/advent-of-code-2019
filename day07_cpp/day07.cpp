#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

#include "../shared_cpp/stringtools.cpp"
#include "../intcode_cpp/intcode.cpp"

void Day07(std::string file) {
    std::ifstream in(file);
    std::vector<int64_t> input;
    // Load file data
    while (!in.eof()) {
        std::string line;
        std::getline(in, line);
        auto numStr = split(line,',');
        for (auto & num : numStr) {
            input.push_back(std::stoi(num));
        }
    }

    // Make Amps
    std::vector<IntcodeComputer> amps(5, input);

    // Part 1
    std::vector<int64_t> settings1 = {0, 1, 2, 3, 4};
    int64_t part1 = 0;
    do {
        // Reset each computer and set initial inputs
        for (size_t i = 0; i < amps.size(); i++) {
            amps[i].reset();
            amps[i].push_input(settings1[i]);
        }
        
        // Run each computer
        int64_t last = 0;
        for (size_t i = 0; i < amps.size(); i++) {
            amps[i].push_input(last);
            amps[i].run();
            last = amps[i].pop_output();
        }
        part1 = std::max(part1, last);
    } 
    while (std::next_permutation(settings1.begin(), settings1.end()));
    printf("Part 1: %lld\n", part1); // 47064
    
    // Part 2
    std::vector<int64_t> settings2 = {5, 6, 7, 8, 9};
    int64_t part2 = 0;
    do {

        // Reset each computer and set initial inputs
        for (size_t i = 0; i < amps.size(); i++) {
            amps[i].reset();
            amps[i].push_input(settings2[i]);
        }
        
        // Run each computer
        int64_t last = 0;
        bool done = false;
        while (!done) {
            for (size_t i = 0; i < amps.size(); i++) {
                amps[i].push_input(last);
                amps[i].run();
                if (!amps[i].active) { done = true; break; }
                last = amps[i].last_output();
            }
        }
        part2 = std::max(part2, last);
    } 
    while (std::next_permutation(settings2.begin(), settings2.end()));
    printf("Part 2: %lld\n", part2); // 4248984

}

int main(int argc, char** argv) {
    // Check for input file
    if (argc != 2) {
        printf("Must specify file at command line.\n");
        return -1;
    }

    Day07(argv[1]);
    return 0;
}