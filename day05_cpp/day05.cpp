#include <iostream>
#include <fstream>
#include <vector>

#include "../shared_cpp/stringtools.cpp"
#include "../intcode_cpp/intcode.cpp"

void Day05(std::string file) {
    std::ifstream in(file);
    std::vector<int64_t> input;
    // Load file data
    while (!in.eof()) {
        std::string line;
        std::getline(in, line);
        auto numStr = split(line,',');
        for (auto & num : numStr) {
            input.push_back(std::stoi(num));
        }
    }

    auto vm = IntcodeComputer(input);
    
    // Part 1
    vm.push_input(1);
    vm.run();
    int64_t part1 = vm.last_output();
    printf("Part 1: %lld\n", part1); // 7265618

    // Part 2
    vm.reset();
    vm.push_input(5);
    vm.break_on_out = false;
    vm.run();
    int64_t part2 = vm.last_output();
    printf("Part 2: %lld\n", part2); // 7731427

}

int main(int argc, char** argv) {
    // Check for input file
    if (argc != 2) {
        printf("Must specify file at command line.\n");
        return -1;
    }

    Day05(argv[1]);
    return 0;
}